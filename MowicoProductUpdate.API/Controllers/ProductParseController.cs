﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductUpdate.API.Models;

namespace MowicoProductUpdate.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductParseController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;

        public ProductParseController(ICategoryService categoryService, IProductService productService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        // GET: api/ProductParse
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ProductParse/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ProductParse
        [HttpPost]
        public void Post([FromBody] ProductParseRequestModel request)
        {
            //burada parse islemi yapilacak
            //mroductService.GetProducts();
            _productService.ParseProducts(request.MerchantId, request.ProductXmlLink);
            _categoryService.GetCategories(request.MerchantId);
            //productService.ParseProducts(request.MerchantId, request.ProductXmlLink);
        }

        // PUT: api/ProductParse/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
