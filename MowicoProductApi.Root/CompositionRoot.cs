﻿using Microsoft.Extensions.DependencyInjection;
using MowicoCategoryApi.Data.Repository;
using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Root
{
    public class CompositionRoot
    {
        public CompositionRoot() { }

        public static void InjectDependencies(IServiceCollection services)
        {            
            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            //services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
