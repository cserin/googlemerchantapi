﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MowicoMerchant.API.Models;
using MowicoProductApi.Business.Interfaces;
using Newtonsoft.Json;

namespace MowicoMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MerchantController
    {
        private readonly IMerchantService _merchantService;

        public MerchantController(IMerchantService merchantService)
        {
            //headerdan merchantId
            _merchantService = merchantService;
            var merchantId = "mocal";
            _merchantService.SetMerchantId(merchantId);
        }

        // GET: api/Merchant
        [HttpGet]
        public string Get()
        {
            try
            {
                return JsonConvert.SerializeObject(_merchantService.GetMerchants());
            }
            catch (System.Exception)
            {
                //Serialization Error
                throw;
            }
        }

        // GET: api/Merchant/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(string id)
        {
            try
            {
                var merchant = _merchantService.GetMerchantByIdAsync(id);
                if (merchant == null)
                {
                    return "Merchant not found.";
                }
                return JsonConvert.SerializeObject(merchant);
            }
            catch (System.Exception)
            {
                //Serialization Error
                throw;
            }
        }

        // POST: api/Merchant
        [HttpPost]
        public void Post([FromBody] MerchantModel request)
        {
            _merchantService.InsertNewMerchantAsync(request.ToDto());
        }

        // PUT: api/Merchant/5
        [HttpPut("{merchantId}")]
        public void Put([FromBody]MerchantModel request)
        {
            _merchantService.UpdateMerchantAsync(request.ToDto());
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{merchantId}")]
        public void Delete(string merchantId)
        {
            _merchantService.DeleteMerchantAsync(merchantId);
        }
    }
}
