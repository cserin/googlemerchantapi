﻿using MowicoProductApi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MowicoMerchant.API.Models
{
    public class MerchantModel
    {
        public string MerchantId { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public bool HasCustomTaxonomy { get; set; }
        public bool IsProductListParsed { get; set; }
        public bool IsTaxonomyParsed { get; set; }
        public bool AreProductsMapped { get; set; }


        public MerchantDTO ToDto()
        {
            return new MerchantDTO()
            {
                Name = this.Name,
                IsEnabled = this.IsEnabled,
                Id = this.MerchantId,
                AreProductsMapped = this.AreProductsMapped,
                HasCustomTaxonomy = this.HasCustomTaxonomy,
                IsProductListParsed = this.IsProductListParsed,
                IsTaxonomyParsed = this.IsTaxonomyParsed
            };
        }
    }
}
