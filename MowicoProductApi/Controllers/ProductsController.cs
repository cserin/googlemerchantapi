﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository;

namespace MowicoProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController
    {
        private readonly IProductService productService;

        public ProductsController(IProductService _productService)
        {
            this.productService = _productService;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<string> Get()
        {
            productService.InsertProduct("mocal");
            return new string[] { "value1", "value2" };
        }

        // GET: api/Products/5
        [HttpGet("{id}", Name = "Get")]
        public void Get(string merchantId, string productsXml)
        {
            productService.InsertProduct(merchantId);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
