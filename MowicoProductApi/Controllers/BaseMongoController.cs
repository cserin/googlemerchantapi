﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Data;
using MowicoProductApi.Data.Repository;

namespace MowicoProductApi.Controllers
{
    public abstract class BaseMongoController<TModel> : ControllerBase where TModel : MongoBaseModel
    {
        //public BaseMongoRepository<TModel> _baseMongoRepository { get; set; }

        //public BaseMongoController(BaseMongoRepository<TModel> baseMongoRepository)
        //{
        //    _baseMongoRepository = baseMongoRepository;
        //}

        //[HttpGet("id")]
        //public virtual ActionResult GetModel(string id)
        //{
        //    return Ok(this._baseMongoRepository.GetById(id));
        //}

        //[HttpGet]
        //public virtual ActionResult GetModelList()
        //{
        //    return Ok(this._baseMongoRepository.GetList<TModel>());
        //}

        //[HttpPost]
        //public virtual ActionResult AddModel(TModel model)
        //{
        //    return Ok(this._baseMongoRepository.Create(model));
        //}

        //[HttpPut]
        //public virtual ActionResult UpdateModel(string id, TModel model)
        //{
        //    this._baseMongoRepository.Update(id, model);
        //    return Ok();
        //}

        //[HttpDelete("{id}")]
        //public virtual void DeleteModel(string id)
        //{
        //    this._baseMongoRepository.Delete(id);
        //}


    }
}