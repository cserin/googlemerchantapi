using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MowicoCategoryApi.Data.Repository;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Business.Services;
using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository;

namespace MowicoProductApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //string mongoConnectionString = this.Configuration.GetConnectionString("MongoConnectionString");
            services.AddAutoMapper(typeof(Product), typeof(string));
            services.AddControllers();
            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICategoryService, CategoryService>();
            //services.AddSingleton<IProductService, ProductService>();
            //services.AddScoped<IMongoDbAccess>(ctx =>
            //   new MongoDbAccess(ctx.GetRequiredService<IOptions<DataMappingDatabaseSettings>>().Value));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
