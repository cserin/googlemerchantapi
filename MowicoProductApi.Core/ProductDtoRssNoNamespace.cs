﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Xml.Serialization;

namespace MowicoProductApi.Core
{
    [XmlRoot(ElementName = "rss")]
    public class ProductDtoRssNoNamespace
    {
        [XmlElement(ElementName = "channel")]
        public ChannelFeed Channel { get; set; }


        public class ChannelFeed
        {
            [XmlElement(ElementName = "title")]
            public string Title { get; set; }

            [XmlElement(ElementName = "item")]
            public List<ItemFeed> Item { get; set; }

            [XmlType(TypeName = "item"), Serializable]
            public class ItemFeed
            {
                [MaxLength(50)]
                [XmlElement(ElementName = "id", Namespace = "http://base.google.com/ns/1.0")]
                public string id { get; set; }

                [MaxLength(50)]
                [XmlElement(ElementName = "shipping", Namespace = "http://base.google.com/ns/1.0")]
                public List<Shipping> shipping { get; set; }

                [MaxLength(150)]
                [XmlElement(ElementName = "title")]
                public string title { get; set; }

                [MaxLength(5000)]
                [XmlElement(ElementName = "description")]
                public string description { get; set; }
                [XmlElement(ElementName = "link")]
                public string link { get; set; }
                [XmlElement(ElementName = "image_link", Namespace = "http://base.google.com/ns/1.0")]
                public string image_link { get; set; }

                [MaxLength(2000)]
                [XmlElement(ElementName = "additional_image_link", Namespace = "http://base.google.com/ns/1.0")]
                public List<string> additional_image_link { get; set; }
                [MaxLength(2000)]
                [XmlElement(ElementName = "mobile_link", Namespace = "http://base.google.com/ns/1.0")]
                public string mobile_link { get; set; }
                [XmlElement(ElementName = "availability", Namespace = "http://base.google.com/ns/1.0")]
                public string availability { get; set; }

                [MaxLength(25)]
                [XmlElement(ElementName = "availability_date", Namespace = "http://base.google.com/ns/1.0")]
                public string availability_date { get; set; }
                [XmlElement(ElementName = "costs_of_goods_sold", Namespace = "http://base.google.com/ns/1.0")]
                public string costs_of_goods_sold { get; set; }
                [XmlElement(ElementName = "expiration_date", Namespace = "http://base.google.com/ns/1.0")]
                public string expiration_date { get; set; }
                [XmlElement(ElementName = "price", Namespace = "http://base.google.com/ns/1.0")]
                public string price { get; set; }
                [XmlElement(ElementName = "sale_price", Namespace = "http://base.google.com/ns/1.0")]
                public string sale_price { get; set; }
                [XmlElement(ElementName = "sale_price_effective_date", Namespace = "http://base.google.com/ns/1.0")]
                public string sale_price_effective_date { get; set; }
                [XmlElement(ElementName = "unit_pricing_base_measure", Namespace = "http://base.google.com/ns/1.0")]
                public string unit_pricing_base_measure { get; set; }
                [XmlElement(ElementName = "unit_pricing_measure", Namespace = "http://base.google.com/ns/1.0")]
                public string unit_pricing_measure { get; set; }
                [XmlElement(ElementName = "installment", Namespace = "http://base.google.com/ns/1.0")]
                public string installment { get; set; }
                [XmlElement(ElementName = "subscription_cost", Namespace = "http://base.google.com/ns/1.0")]
                public string subscription_cost { get; set; }
                [XmlElement(ElementName = "loyalty_points", Namespace = "http://base.google.com/ns/1.0")]
                public string loyalty_points { get; set; }
                [XmlElement(ElementName = "google_product_category", Namespace = "http://base.google.com/ns/1.0")]
                public string google_product_category { get; set; }

                //[MaxLength(750)]
                [XmlElement(ElementName = "product_type", Namespace = "http://base.google.com/ns/1.0")]
                public string product_type { get; set; }
                //[MaxLength(70)]
                [XmlElement(ElementName = "brand", Namespace = "http://base.google.com/ns/1.0")]
                public string brand { get; set; }
                [XmlElement(ElementName = "gtin", Namespace = "http://base.google.com/ns/1.0")]
                public string gtin { get; set; }
                [MaxLength(70)]
                [XmlElement(ElementName = "mpn", Namespace = "http://base.google.com/ns/1.0")]
                public string mpn { get; set; }
                [XmlElement(ElementName = "identifier_exists", Namespace = "http://base.google.com/ns/1.0")]
                public bool identifier_exists { get; set; }
                [XmlElement(ElementName = "condition", Namespace = "http://base.google.com/ns/1.0")]
                public string condition { get; set; }
                [XmlElement(ElementName = "adult", Namespace = "http://base.google.com/ns/1.0")]
                public bool adult { get; set; }
                [XmlElement(ElementName = "multipack", Namespace = "http://base.google.com/ns/1.0")]
                public int multipack { get; set; }
                [XmlElement(ElementName = "is_bundle", Namespace = "http://base.google.com/ns/1.0")]
                public bool is_bundle { get; set; }
                [XmlElement(ElementName = "energy_efficiency_class", Namespace = "http://base.google.com/ns/1.0")]
                public string energy_efficiency_class { get; set; }
                [XmlElement(ElementName = "min_energy_efficiency_class", Namespace = "http://base.google.com/ns/1.0")]
                public string min_energy_efficiency_class { get; set; }
                [XmlElement(ElementName = "max_energy_efficiency_class", Namespace = "http://base.google.com/ns/1.0")]
                public string max_energy_efficiency_class { get; set; }
                [XmlElement(ElementName = "age_group", Namespace = "http://base.google.com/ns/1.0")]
                public string age_group { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "color", Namespace = "http://base.google.com/ns/1.0")]
                public string color { get; set; }
                [XmlElement(ElementName = "gender", Namespace = "http://base.google.com/ns/1.0")]
                public string gender { get; set; }
                [MaxLength(200)]
                [XmlElement(ElementName = "material", Namespace = "http://base.google.com/ns/1.0")]
                public string material { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "pattern", Namespace = "http://base.google.com/ns/1.0")]
                public string pattern { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "size", Namespace = "http://base.google.com/ns/1.0")]
                public string size { get; set; }
                [XmlElement(ElementName = "size_type", Namespace = "http://base.google.com/ns/1.0")]
                public string size_type { get; set; }
                [XmlElement(ElementName = "size_system", Namespace = "http://base.google.com/ns/1.0")]
                public string size_system { get; set; }
                [MaxLength(50)]
                [XmlElement(ElementName = "item_group_id", Namespace = "http://base.google.com/ns/1.0")]
                public string item_group_id { get; set; }
                [MaxLength(2000)]
                [XmlElement(ElementName = "ads_redirect", Namespace = "http://base.google.com/ns/1.0")]
                public string ads_redirect { get; set; }

                [MaxLength(100)]
                [XmlElement(ElementName = "custom_label_0", Namespace = "http://base.google.com/ns/1.0")]
                public string custom_label_0 { get; set; }

                [MaxLength(100)]
                [XmlElement(ElementName = "custom_label_1", Namespace = "http://base.google.com/ns/1.0")]
                public string custom_label_1 { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "custom_label_2", Namespace = "http://base.google.com/ns/1.0")]
                public string custom_label_2 { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "custom_label_3", Namespace = "http://base.google.com/ns/1.0")]
                public string custom_label_3 { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "custom_label_4", Namespace = "http://base.google.com/ns/1.0")]
                public string custom_label_4 { get; set; }
                [MaxLength(50)]
                [XmlElement(ElementName = "promotion_id", Namespace = "http://base.google.com/ns/1.0")]
                public string promotion_id { get; set; }
                [XmlElement(ElementName = "excluded_destination", Namespace = "http://base.google.com/ns/1.0")]
                public string excluded_destination { get; set; }
                [XmlElement(ElementName = "included_destination", Namespace = "http://base.google.com/ns/1.0")]
                public string included_destination { get; set; }
                [MaxLength(100)]
                [XmlElement(ElementName = "shipping_label", Namespace = "http://base.google.com/ns/1.0")]
                public string shipping_label { get; set; }
                [XmlElement(ElementName = "shipping_weight", Namespace = "http://base.google.com/ns/1.0")]
                public string shipping_weight { get; set; }
                [XmlElement(ElementName = "shipping_height", Namespace = "http://base.google.com/ns/1.0")]
                public string shipping_height { get; set; }
                [XmlElement(ElementName = "shipping_width", Namespace = "http://base.google.com/ns/1.0")]
                public string shipping_width { get; set; }
                [XmlElement(ElementName = "shipping_length", Namespace = "http://base.google.com/ns/1.0")]
                public string shipping_length { get; set; }
                [XmlElement(ElementName = "transit_time_label", Namespace = "http://base.google.com/ns/1.0")]
                [MaxLength(100)]
                public string transit_time_label { get; set; }
                [XmlElement(ElementName = "min_handling_time", Namespace = "http://base.google.com/ns/1.0")]

                public int min_handling_time { get; set; }
                [XmlElement(ElementName = "max_handling_time", Namespace = "http://base.google.com/ns/1.0")]

                public int max_handling_time { get; set; }
                [XmlElement(ElementName = "tax", Namespace = "http://base.google.com/ns/1.0")]
                public List<Tax> tax { get; set; }

                [MaxLength(100)]
                [XmlElement(ElementName = "tax_category", Namespace = "http://base.google.com/ns/1.0")]
                public string tax_category { get; set; }

                [XmlType(TypeName = "shipping"), Serializable]
                public class Shipping
                {
                    [XmlElement(ElementName = "country", Namespace = "http://base.google.com/ns/1.0")]
                    public string Country { get; set; }
                    [XmlElement(ElementName = "service", Namespace = "http://base.google.com/ns/1.0")]
                    public string Service { get; set; }
                    [XmlElement(ElementName = "price", Namespace = "http://base.google.com/ns/1.0")]
                    public string Price { get; set; }
                }

                public class Tax {
                    [XmlElement(ElementName = "country", Namespace = "http://base.google.com/ns/1.0")]
                    public string country { get; set; }
                    [XmlElement(ElementName = "region", Namespace = "http://base.google.com/ns/1.0")]
                    public string region { get; set; }
                    [XmlElement(ElementName = "rate", Namespace = "http://base.google.com/ns/1.0")]
                    public decimal? rate { get; set; }
                    [XmlElement(ElementName = "tax_ship", Namespace = "http://base.google.com/ns/1.0")]
                    public string tax_ship { get; set; }
                }
            }
        }
    }
}
