﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models
{
    public class SearchFilters
    {
        public string Color { get; set; }
        public string Size { get; set; }
        public string Brand { get; set; }
        public PriceRange Price { get; set; }
    }
    public class PriceRange
    {
        public decimal StartRange { get; set; }
        public decimal EndRange { get; set; }
    }
}
