﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models
{
    public class ProductCategoryMapResponseModel
    {
        public int NumberOfProductsMapped { get; set; }
        public int NumberOfCategoriesMapped { get; set; }
    }
}
