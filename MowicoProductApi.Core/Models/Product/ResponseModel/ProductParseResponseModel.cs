﻿using MowicoProductApi.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models.Product.ResponseModel
{
    public class ProductParseResponseModel
    {
        public int InsertedProductCount { get; set; }
    }
}
