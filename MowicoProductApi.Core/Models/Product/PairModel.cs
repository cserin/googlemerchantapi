﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class PairModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public PairModel()
        {

        }
        public PairModel(string id, string name)
        {
            Id = id;
            Name = name;
        }
        public PairModel(int id, string name)
        {
            Id = id.ToString(CultureInfo.InvariantCulture);
            Name = name;
        }

        public override string ToString()
        {
            return string.Join("|", Id, Name);
        }
    }
}
