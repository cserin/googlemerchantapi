﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class ShareContentObject
    {
        public string Url { get; set; }
        public ImageObject Image { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
