﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class CreditCardPaymentInfoModel
    {
        public string Code { get; set; }
        public ImageObject Image { get; set; }
        public string Name { get; set; }
        public int CreditCardPaymentType { get; set; }
        public List<InstallmentModel> Installments { get; set; }
    }
}

