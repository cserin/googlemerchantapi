﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class ProductPropertyObject
    {
        public string PropertyId { get; set; }
        public string Name { get; set; }
        public PairModel SelectedOption { get; set; }
        public List<PairModel> OptionList { get; set; }
    }
}
