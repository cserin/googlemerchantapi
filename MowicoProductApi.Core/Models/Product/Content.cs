﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class Content
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public ImageObject ContentImage { get; set; }
        public int ContentType { get; set; }
        public ShareContentObject ShareContent { get; set; }
    }
}
