﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models.Product.RequestModel
{
    public class ProductRequestModel
    {
        public string CategoryId { get; set; }

        public string ProductId { get; set; }

        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public string SortId { get; set; }

        public SearchFilters SearchFilters { get; set; }

        public ProductRequestModel()
        {
            this.PageNumber = 1;
            this.PageSize = 10;
        }

        public ProductRequestModel(string categoryId, string productId, int pageNumber, int pageSize, string sortId)
        {
            this.CategoryId = categoryId;
            this.ProductId = productId;
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 10 ? 10 : pageSize;
            this.SortId = sortId;
        }
    }
}
