﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class ProductResponseModel
    {
        public ProductModel Product { get; set; }
    }
}
