﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class SubscriptionInfoModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<PairModel> Periods { get; set; }
        public string Frequency { get; set; }
        public string Status { get; set; }
        public List<PairModel> DispatchCounts { get; set; }
        public string SelectedPeriod { get; set; }
        public string SelectedDispatchCounts { get; set; }
    }
}
