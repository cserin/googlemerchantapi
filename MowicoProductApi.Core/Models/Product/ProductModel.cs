﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class ProductModel
    {
        public List<ImageObject> Images { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }// TODO : client'da var mı?
        public bool HasStock { get; set; }
        public List<ProductPropertyObject> ProductPropertyList { get; set; }
        public List<ProductPropertyObject> ProductInfoList { get; set; }
        public decimal PreviousPrice { get; set; }
        public bool IsCampaign { get; set; }// TODO : client'da var mı?

        public List<CreditCardPaymentInfoModel> InstallmentInfo { get; set; }
        public string ShipmentInformation { get; set; }
        public bool IsShipmentFree { get; set; }
        public List<Content> Poppers { get; set; } // TODO : type sıkınılı, düzgün bir modele geçilmeli

        public List<string> InfoText { get; set; } // TODO : client'da var mı?
        public string Currency { get; set; }
        public bool IsFavorite { get; set; }
        public List<PairModel> AdditionalTexts { get; set; }
        public string SpecialText { get; set; }
        public List<CustomInputObject> CustomInputs { get; set; }
        public int? StockAmount { get; set; }
        public SubscriptionInfoModel SubscriptionInfo { get; set; }
        public bool FetchIfNeededVariant { get; set; }
        public string RebatePercentage { get; set; }
        public List<ProductPropertyObject> ProductSelections { get; set; }
        public CustomInputConstraintModel Constraint { get; set; }
    }
}
