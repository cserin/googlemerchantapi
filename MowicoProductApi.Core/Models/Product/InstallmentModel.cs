﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class InstallmentModel
    {
        public int Installment { get; set; }
        public string Name { get; set; } // TODO : client'da var mı?
        public string Price { get; set; }
        public string Currency { get; set; }
        public string InstallmentAmount { get; set; }
    }
}
