﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class CustomInputObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsRequired { get; set; }
        public string MinLength { get; set; }
        public string MaxLength { get; set; }
        public string Type { get; set; }
        public string FileContent { get; set; }
    }
}
