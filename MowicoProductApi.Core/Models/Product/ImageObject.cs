﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class ImageObject
    {
        public string Url { get; set; }
        public string ErrorImageUrl { get; set; }
    }
}
