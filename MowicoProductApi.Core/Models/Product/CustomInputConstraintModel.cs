﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.ResponseModels
{
    public class CustomInputConstraintModel
    {
        public int Size { get; set; }
        public string ReadableSize { get; set; }
        public List<string> Extensions { get; set; }
        public List<string> MimeTypes { get; set; }
    }
}
