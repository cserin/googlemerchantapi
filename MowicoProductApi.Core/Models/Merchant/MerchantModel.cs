﻿using MowicoProductApi.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models
{
    public class MerchantModel : BaseModel
    {
        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string MerchantId { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public bool HasCustomTaxonomy { get; set; }
        public bool IsProductListParsed { get; set; }
        public bool IsTaxonomyParsed { get; set; }
        public bool AreProductsMapped { get; set; }


    }
}
