﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models.Base
{
    public abstract class BaseModel
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorDetails { get; set; }

        public BaseModel()
        {
            this.ErrorCode = 0;
            this.ErrorDetails = string.Empty;
            this.ErrorMessage = string.Empty;
        }

        public BaseModel(int errorCode, string ErrorMessage, string errorDetails)
        {
            this.ErrorCode = errorCode;
            this.ErrorMessage = ErrorMessage;
            this.ErrorDetails = errorDetails;
        }

    }
}
