﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models.Products
{
    public class Product
    {
        /// <summary>
        /// Id of a product
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Name of a product
        /// </summary>
        public string productName { get; set; }
        /// <summary>
        /// List price of a product, this is the price that customers pay
        /// </summary>
        public Price listPrice { get; set; }
        /// <summary>
        /// Default image url if any of product images has errors
        /// </summary>
        public string noImageUrl { get; set; }
        /// <summary>
        /// Headline is the subtitle of a product, it is below the product an in design, services returns sku number usually
        /// </summary>
        public string headline { get; set; }
        /// <summary>
        /// Older price if there is a discount for the product
        /// </summary>
        public Price strikeoutPrice { get; set; }
        /// <summary>
        /// Show a campaign patch on the product if it is campaign product
        /// </summary>
        public bool IsCampaign { get; set; }
        /// <summary>
        /// Credit card installments table
        /// </summary>
        public List<CreditCardInstallments> creditCardInstallments { get; set; }
        /// <summary>
        /// Show "Not in stock" if true
        /// </summary>
        public bool inStock { get; set; }
        /// <summary>
        /// Additional shipment information for supply date or any shipment info
        /// </summary>
        public string shipmentInformation { get; set; }
        /// <summary>
        /// Show a patch if shipping is free
        /// </summary>
        public bool isShipmentFree { get; set; }
        /// <summary>
        /// Features of a product grouped as a list. This is usually used for technical
        /// details of product. You can set it null if you do not have this type of
        /// content.
        /// </summary>
        public List<Group> features { get; set; }
        /// <summary>
        /// Variants of product, variants are grouped variant lists with name, see
        /// example in sample response             
        /// </summary>
        public List<Group> variants { get; set; }
        /// <summary>
        /// List of text for important temporary informations for the product.
        /// </summary>
        public List<string> shoutOutTexts { get; set; }
        /// <summary>
        /// We have two types of product action. One of them is popper. Poppers are small
        /// images below the product image, about free shipping or delivery in one day.
        /// Poppers can be define as type is set to "popper" and image is the small image
        /// of the popper. We can also add a label about discounts on product listing. 
        /// Discounts can be defined as type is set "discount" and text is set to
        /// percentage of discount or any other text value about discount.
        /// </summary>
        public List<Action> actions { get; set; }
        /// <summary>
        /// Picture of the product for product listing.
        /// </summary>
        public string picture { get; set; }
        /// <summary>
        /// Picture list for product detail, you should add url in "picture" field if you 
        /// want to display that picture in product detail.
        /// </summary>
        public List<Picture> pictures { get; set; }
        /// <summary>
        /// Product detail as HTML CONTENT or plain text. You can leave empty or null if
        /// you do not have this content. 
        /// </summary>
        public string productDetailUrl { get; set; } // TODO : ismini değiştir
        /// <summary>
        /// Full URL of the product on the website. This will be used for social media 
        /// sharing.
        /// </summary>
        public string productUrl { get; set; }
        /// <summary>
        /// Points to be rewarded when you buy this product
        /// </summary>
        public int points { get; set; }
        /// <summary>
        /// What include for one unit of product to be sell, optional.
        /// </summary>
        public string unit { get; set; }
        /// <summary>
        /// If the product is selected as favorite by user. You can set false if there is no user logged in.
        /// </summary>
        public bool isFavorite { get; set; }
        /// <summary>
        /// If the product is selected for back in stock subscription by user. You can set false if there is no user logged in.
        /// </summary>
        public bool isBackInStockSubscription { get; set; }
        /// <summary>
        /// Selected variants for back in stock subscription.
        /// </summary>
        public Group backInStockSubSelectedVariant { get; set; }
        /// <summary>
        /// Additional texts for displaying additional information of products.
        /// </summary>
        public List<AdditionalText> additionaltexts { get; set; }
        /// <summary>
        /// Product specific description field, optional.
        /// </summary>
        public string specialText { get; set; }
        /// <summary>
        /// Product specific input text field, optional.
        /// </summary>
        public List<CustomInput> customInputs { get; set; }
        /// <summary>
        /// Product stock count value, optional.
        /// </summary>
        public int? stockAmount { get; set; }
        /// <summary>
        /// Product specific subscription options, optional.
        /// </summary>
        public SubscriptionInfo subscription { get; set; }
        public bool hasVariant { get; set; }
        public string rebatePercentage { get; set; }
        public List<Group> selections { get; set; }
        public CustomInputConstraint constraint { get; set; }
    }

    public class Price
    {
        /// <summary>
        /// Price in actual currency
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// Actual currency
        /// </summary>
        public string currency { get; set; }
        /// <summary>
        /// Price in default currency
        /// </summary>
        public string amountDefaultCurrency { get; set; }
    }

    public class CreditCardInstallments
    {
        /// <summary>
        /// Image of credit card
        /// </summary>
        public string image { get; set; }
        /// <summary>
        /// type or id of the credit card, this value is needed for setting this credit 
        /// card as a payment type 
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 3D payment type of the credit card. 
        /// 0: Only 2D payment
        /// 1: Only 3D payment
        /// 2: Both 2D and 3D can be used
        /// 3: If 3D fails transaction continues with 2D
        /// </summary>
        public int threeDStatus { get; set; }
        /// <summary>
        /// Card name to be displayed
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// Max installment number.
        /// </summary>
        public int installmentNumber { get; set; }
        /// <summary>
        /// All installments a customer can use
        /// </summary>
        public List<Installment> installments { get; set; }
    }

    public class Installment
    {
        /// <summary>
        /// Number of installment
        /// </summary>
        public int installmentNumber { get; set; }
        /// <summary>
        /// Paymnet per installment
        /// </summary>
        public decimal installmentPayment { get; set; }
        /// <summary>
        /// Total payment if a customer chooses this payment type
        /// </summary>
        public decimal total { get; set; }
    }

    public class Group
    {
        /// <summary>
        /// Name of group to display
        /// </summary>
        public string groupName { get; set; }
        /// <summary>
        /// Id of group
        /// </summary>
        public string groupId { get; set; }
        /// <summary>
        /// Feature list of group
        /// </summary>
        public List<GroupFeature> features { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Group group = (Group)obj;
                return groupId == group.groupId && groupName == group.groupName && features == group.features;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class GroupFeature
    {
        /// <summary>
        /// Name of feature to display
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// Value of feature, when it is selected this value is posted to services
        /// </summary>
        public string value { get; set; }
        /// <summary>
        /// selected of feature
        /// </summary>
        public string selected { get; set; }
        public List<GroupFeature> VariantCombinationSet { get; set; }
    }

    public class Action
    {
        /// <summary>
        /// Type of action, this value should be "product", "search" or "webview"
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// Image of action, this value is needed for displaying product actions as image
        /// </summary>
        public string image { get; set; }
        /// <summary>
        /// Search text when type is "search", can be empty or null otherwise
        /// </summary>
        public string text { get; set; }
        /// <summary>
        /// Product id value when type is "product", can be empty or null otherwise
        /// </summary>
        public string productId { get; set; }
        /// <summary>
        /// URL text when type is "webview", can be empty or null otherwise
        /// </summary>
        public string href { get; set; }
        /// <summary>
        /// Category id value when type is "category", can be empty or null otherwise,
        /// not released yet
        /// </summary>
        public string categoryId { get; set; }
    }

    public class Picture
    {
        /// <summary>
        /// url of the picture
        /// </summary>
        public string url { get; set; }
    }

    public class AdditionalText
    {
        /// <summary>
        /// Type of text. Key must be match with middleware. Please inform us for key values.
        /// </summary>
        public string key { get; set; }
        /// <summary>
        /// Text to display.
        /// </summary>
        public string text { get; set; }
    }

    public class CustomInput
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool isRequired { get; set; }
        public string minLength { get; set; }
        public string maxLength { get; set; }
        public string value { get; set; }
        public string Type { get; set; }
        public string FileContent { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                CustomInput custom = (CustomInput)obj;
                return id == custom.id && value == custom.value && FileContent == custom.FileContent && name == custom.name;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    public class SubscriptionInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public List<Period> periods { get; set; }
        public string frequency { get; set; }
        public string status { get; set; }
        public List<DispatchCount> dispatch_counts { get; set; }
        public int continuous_sending { get; set; }
        public string selected_period { get; set; }
        public string selected_dispatch_counts { get; set; }
    }

    public class Period
    {
        public string value { get; set; }
        public string title { get; set; }
    }

    public class DispatchCount
    {
        public string value { get; set; }
    }

    public class CustomInputConstraint
    {
        public Constraint Constraint { get; set; }
    }

    public class Constraint
    {
        public int Size { get; set; }
        public string ReadableSize { get; set; }
        public List<string> Extensions { get; set; }
        public List<string> MimeTypes { get; set; }
    }

    public class Filter
    {
        /// <summary>
        /// Id of filter, used for sending search requests with filter
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// name of filter to display
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// selected filter item id, should be set after a request if a filter item
        /// is selected for this filter
        /// </summary>
        public string selectedItemId { get; set; }
        /// <summary>
        /// if filter type is range (2), start value of price range
        /// </summary>
        public int rangeStart { get; set; }
        /// <summary>
        /// if filter type is range (2), end value of price range
        /// </summary>
        public int rangeEnd { get; set; }
        /// <summary>
        /// SingleOption = 0,
        /// MultipleOption = 1,
        /// Range = 2,
        /// </summary>
        public FilterType FilterType { get; set; }
        /// <summary>
        /// list of items to be selected
        /// </summary>
        public List<FilterItem> items { get; set; }
    }
    public class FilterItem
    {
        /// <summary>
        /// Id of filter item
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Name of filter item to display
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// number of products 
        /// </summary>
        public int productCount { get; set; }
    }

    public enum FilterType
    {
        SingleOption = 0,
        MultipleOption = 1,
        Range = 2,
    }

    public class Sort
    {
        /// <summary>
        /// Name of the sort
        /// </summary>
        public string displayName { get; set; }
        /// <summary>
        /// Value of the sort, this value is used in product list requests
        /// </summary>
        public string value { get; set; }
    }
}
