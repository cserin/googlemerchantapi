﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models.Products
{
    public class Category
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsRoot { get; set; }
        public bool IsLeaf { get; set; }
        public string ParentCategoryId { get; set; }
        public List<Category> Children { get; set; }
    }
}
