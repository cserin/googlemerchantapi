﻿using MowicoProductApi.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Models.Products
{
    public class ProductListModel
    {
        /// <summary>
        /// Total number of product for this search
        /// </summary>
        public int totalResultCount { get; set; }
        /// <summary>
        /// Actual page number of pagination
        /// </summary>
        public int pageNumber { get; set; }
        /// <summary>
        /// Pagination size
        /// </summary>
        public int pageSize { get; set; }
        /// <summary>
        /// List of found products
        /// </summary>
        public List<Product> products { get; set; }
        /// <summary>
        /// Available filter list of this search
        /// </summary>
        public List<Filter> filters { get; set; }
        /// <summary>
        /// Avaliable sort options for this search
        /// </summary>
        public List<Sort> sortList { get; set; }
        /// <summary>
        /// List of categories include these products.
        /// </summary>
        public List<Category> categories { get; set; }
    }
}
