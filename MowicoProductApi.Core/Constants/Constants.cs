﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Constants
{
    public class Constants
    {
        public static readonly string CollectionBackupPath = @"C:\MowicoMerchant\Backup\";
        public static readonly string GoogleMerchantCategoryExcelFilePath = @"C:\MowicoMerchant\GoogleMerchantExcel\GoogleCategoryTree.xls";
        //names of the tables in DB
        public static readonly string ProductCollectionName = "products";
        public static readonly string CategoryCollectionName = "categories";
        public static readonly string MerchantCollectionName = "merchants";
        //tables will be stored by these names e.g products_en if language is English
        public static readonly string EnglishSuffix = "_en";
        public static readonly string TurkishSuffix = "_tr";
    }
}
