﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core
{
    public class CategoryDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ParentCategoryId { get; set; }
        public bool IsEnabled { get; set; }

        public static CategoryDTO FromDto(Category category)
        {
            return new CategoryDTO()
            {
                Id = category.Id,
                Name = category.Name,
                ParentCategoryId = category.ParentCategoryId,
                IsEnabled = category.IsEnabled
            };
        }
    }
}
