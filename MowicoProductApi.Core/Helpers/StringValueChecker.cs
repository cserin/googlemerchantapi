﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MowicoProductApi.Core.Helpers
{
    public class StringValueChecker
    {
        public static bool HasStringValues(string input)
        {
            return (Regex.Matches(input, @"[a-zA-Z]").Count > 1) ? true : false;
        }
    }
}
