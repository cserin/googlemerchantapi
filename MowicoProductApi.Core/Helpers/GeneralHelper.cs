﻿using MongoDB.Bson;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using static MowicoProductApi.Data.Models.Product;

namespace MowicoProductApi.Core.Helpers
{
    public class GeneralHelper
    {
        public static SalePriceEffectiveDateInfo ConvertToSaleDate(string date)
        {
            SalePriceEffectiveDateInfo salePriceInfo = new SalePriceEffectiveDateInfo();
            if (date.Contains('/'))
            {
                var seperatedDates = date.Split('/');
                salePriceInfo = new SalePriceEffectiveDateInfo() { StartDate = DateTime.Parse(seperatedDates[0]), EndDate = DateTime.Parse(seperatedDates[0]) };
            }
            else
            {
                salePriceInfo = new SalePriceEffectiveDateInfo() { StartDate = DateTime.Parse(date) };
            }
            return salePriceInfo;
        }

        public static Tuple<decimal, string> ConvertStringToDecimal(string salePrice)
        {
            decimal money = 0;
            string currency = "";
            if (salePrice == null)
            {
                return new Tuple<decimal, string>(money, currency);
            }
            string[] priceAndCurrency = salePrice.Split(" ");
            if (!string.IsNullOrEmpty(priceAndCurrency[0]))
            {
                priceAndCurrency[0] = priceAndCurrency[0].Replace(".", string.Empty);
                money = decimal.Parse(priceAndCurrency[0]);
            }
            if (!string.IsNullOrEmpty(priceAndCurrency[1]))
            {
                currency = priceAndCurrency[1].ToString();
            }
            return new Tuple<decimal, string>(money, currency);
        }

        public static Price ConvertToPriceInfo(string salePrice)
        {
            Price priceInfo = new Price();
            if (salePrice == null)
            {
                return priceInfo;
            }
            string[] priceAndCurrency = salePrice.Split(" ");
            if (!string.IsNullOrEmpty(priceAndCurrency[0]))
            {
                priceAndCurrency[0] = priceAndCurrency[0].Replace(".", string.Empty);
                priceInfo.Amount = decimal.Parse(priceAndCurrency[0], new NumberFormatInfo() { NumberDecimalSeparator = "," });
            }
            if (!string.IsNullOrEmpty(priceAndCurrency[1]))
            {
                priceInfo.Currency = priceAndCurrency[1].ToString();
            }
            return priceInfo;
        }

        public static int CalculateDiscountRate(decimal initialPrice, decimal discountPrice)
        {
            return (int)(discountPrice * 100 / initialPrice);
        }

        public static string GetCollectionExportPath(string collection, string merchantId)
        {
            return collection switch
            {
                "products" => Constants.Constants.CollectionBackupPath + Constants.Constants.ProductCollectionName + '_' + merchantId + '_' + DateTime.Now.ToString("yyyyMMdd") + ".json",
                "categories" => Constants.Constants.CollectionBackupPath + Constants.Constants.CategoryCollectionName + '_' + merchantId + '_' + DateTime.Now.ToString("yyyyMMdd") + ".json",
                _ => null,
            };
        }

        public static bool CheckObjectId(string objectId)
        {
            ObjectId objId;
            if (!ObjectId.TryParse(objectId, out objId))
            {
                return false;
            }
            return true; ;
        }

    }
}
