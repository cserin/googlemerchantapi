﻿using MongoDB.Bson;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MowicoProductApi.Core.Mapper
{
    public class ProductCategoryMapper
    {
        public static List<Product> MapProductsToCategories(IQueryable<Product> products, IQueryable<Category> categories)
        {
            try
            {
                //Stopwatch sw = new Stopwatch();
                //sw.Start();                        
                List<Category> missingCategories = new List<Category>();
                var productList = products.ToList();
                var categoryList = categories.ToList();
                //nested loops
                foreach (var product in productList)
                {
                    var categoryListSeperated = product.GoogleProductCategory.Split('>').Select(c => c.Trim()).ToList();
                    List<string> categoryIdList = new List<string>();
                    for (int i = 0; i < categoryListSeperated.Count(); i++)
                    {
                        var selectedCat = categoryList.Where(c => c.Name == categoryListSeperated[i]).FirstOrDefault()?.Id;
                    }
                    foreach (var category in categoryListSeperated)
                    {
                        var selectedCat = categoryList.Where(c => c.Name == category).FirstOrDefault()?.Id;
                        if (selectedCat == null)
                        {
                            //if null, it means there is no category corresponding to Google Taxonomy list. create a new category and log it.
                            Category missingCategory = new Category()
                            {
                                Id = ObjectId.GenerateNewId().ToString(),
                                Name = category
                            };
                            missingCategories.Add(missingCategory);
                        }
                        categoryIdList.Add(selectedCat?.ToString());
                    }
                    //producti guncelle
                    product.ProductCategoryList = categoryIdList;
                }
                //final list of missing categories
                var missingCatesgoriesDistinct = missingCategories.GroupBy(c => c).Select(c => c.FirstOrDefault());
                //sw.Stop();
                //var elapsed = sw.Elapsed;
                return productList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
