﻿using AutoMapper;
using GoogleMerchant.API.Models.ResponseModels;
using MowicoProductApi.Core.Helpers;
using MowicoProductApi.Core.Models.Products;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using static MowicoProductApi.Data.Models.Product;

namespace MowicoProductApi.Core.Mapper
{
    public class ProductMapper
    {
        public static Data.Models.Product FromDto(ProductDtoRss.ChannelFeed.ItemFeed feed)
        {
            Data.Models.Product p = new Data.Models.Product();
            p.AdditionalImageLink = feed.additional_image_link;
            p.AdsRedirect = feed.ads_redirect;
            p.Adult = feed.adult;
            p.AgeGroup = feed.age_group;
            p.Availability = feed.availability;
            p.AvailabilityDate = (string.IsNullOrEmpty(feed.availability_date) ? (DateTime?)null : DateTime.Parse(feed.availability_date));
            p.Brand = feed.brand;
            p.Color = feed.color;
            p.Condition = feed.condition;
            p.CostsOfGoodsSold = feed.costs_of_goods_sold;
            p.Description = feed.description;
            p.ExcludedDestination = feed.excluded_destination;
            p.ExpirationDate = (string.IsNullOrEmpty(feed.expiration_date) ? (DateTime?)null : DateTime.Parse(feed.expiration_date));
            p.Gender = feed.gender;
            p.GoogleProductCategory = feed.google_product_category;
            p.Gtin = feed.gtin;
            p.IdentifierExists = feed.identifier_exists;
            p.ImageLink = feed.image_link;
            p.IncludedDestination = feed.included_destination;
            p.Installment = feed.installment;
            p.IsBundle = feed.is_bundle;
            p.ItemGroupId = feed.item_group_id;
            p.Link = feed.link;
            p.LoyaltyPoints = feed.loyalty_points;
            p.Material = feed.material;
            p.MaxHandlingTime = feed.max_handling_time;
            p.MinHandlingTime = feed.min_handling_time;
            p.MobileLink = feed.mobile_link;
            p.Mpn = feed.mpn;
            p.Multipack = feed.multipack;
            p.Pattern = feed.pattern;
            p.PriceInfo = GeneralHelper.ConvertToPriceInfo(feed.price);
            p.ProductType = feed.product_type;
            p.PromotionId = feed.promotion_id;
            p.SalePriceInfo = GeneralHelper.ConvertToPriceInfo(feed.sale_price);
            p.SalePriceEffectiveDate = (string.IsNullOrEmpty(feed.sale_price_effective_date)) ? null : GeneralHelper.ConvertToSaleDate(feed.sale_price_effective_date);
            p.Shipping = feed.shipping.Select(s => new Data.Models.Product.ShippingInfo() { Country = s.Country, Price = s.Price, Service = s.Service }).ToList();
            p.EnergyEfficiencyClass = new Data.Models.Product.EnergyEfficiency()
            {
                EnergyEfficiencyClass = feed.energy_efficiency_class,
                MaxEnergyEfficiencyClass = feed.max_energy_efficiency_class,
                MinEnergyEfficiencyClass = feed.min_energy_efficiency_class
            };
            p.CustomLabel = new Data.Models.Product.CustomLabels()
            {
                CustomLabel0 = feed.custom_label_0,
                CustomLabel1 = feed.custom_label_1,
                CustomLabel2 = feed.custom_label_2,
                CustomLabel3 = feed.custom_label_3,
                CustomLabel4 = feed.custom_label_4,
            };

            p.ShippingLabel = feed.shipping_label;
            p.Size = feed.size;
            p.SizeSystem = feed.size_system;
            p.SizeType = feed.size_type;
            p.SubscriptionCost = feed.subscription_cost;
            var taxInfos = new List<Data.Models.Product.Tax>();
            if (feed.tax != null && feed.tax.Count > 0)
            {
                foreach (var tax in feed.tax)
                {
                    taxInfos.Add(new Data.Models.Product.Tax()
                    {
                        country = tax.country,
                        rate = tax.rate,
                        tax_ship = tax.tax_ship,
                        region = tax.region
                    });
                }
            }
            p.TaxInfo = taxInfos;
            p.TaxCategory = feed.tax_category;
            p.Title = feed.title;
            p.TransitTimeLabel = feed.transit_time_label;
            p.UnitPricingBaseMeasure = feed.unit_pricing_base_measure;
            p.UnitPricingMeasure = feed.unit_pricing_measure;
            return p;
        }

        public static Data.Models.Product FromDto(ProductDtoRssNoNamespace.ChannelFeed.ItemFeed feed)
        {
            Data.Models.Product p = new Data.Models.Product();
            p.AdditionalImageLink = feed.additional_image_link;
            p.AdsRedirect = feed.ads_redirect;
            p.Adult = feed.adult;
            p.AgeGroup = feed.age_group;
            p.Availability = feed.availability;
            p.AvailabilityDate = (string.IsNullOrEmpty(feed.availability_date) ? (DateTime?)null : DateTime.Parse(feed.availability_date));
            p.Brand = feed.brand;
            p.Color = feed.color;
            p.Condition = feed.condition;
            p.CostsOfGoodsSold = feed.costs_of_goods_sold;
            p.Description = feed.description;
            p.ExcludedDestination = feed.excluded_destination;
            p.ExpirationDate = (string.IsNullOrEmpty(feed.expiration_date) ? (DateTime?)null : DateTime.Parse(feed.expiration_date));
            p.Gender = feed.gender;
            p.GoogleProductCategory = feed.google_product_category;
            p.Gtin = feed.gtin;
            p.IdentifierExists = feed.identifier_exists;
            p.ImageLink = feed.image_link;
            p.IncludedDestination = feed.included_destination;
            p.Installment = feed.installment;
            p.IsBundle = feed.is_bundle;
            p.ItemGroupId = feed.item_group_id;
            p.Link = feed.link;
            p.LoyaltyPoints = feed.loyalty_points;
            p.Material = feed.material;
            p.MaxHandlingTime = feed.max_handling_time;
            p.MinHandlingTime = feed.min_handling_time;
            p.MobileLink = feed.mobile_link;
            p.Mpn = feed.mpn;
            p.Multipack = feed.multipack;
            p.Pattern = feed.pattern;
            p.PriceInfo = GeneralHelper.ConvertToPriceInfo(feed.price);
            p.ProductType = feed.product_type;
            p.PromotionId = feed.promotion_id;
            p.SalePriceInfo = GeneralHelper.ConvertToPriceInfo(feed.sale_price);
            p.SalePriceEffectiveDate = (string.IsNullOrEmpty(feed.sale_price_effective_date)) ? null : GeneralHelper.ConvertToSaleDate(feed.sale_price_effective_date);
            p.Shipping = feed.shipping.Select(s => new Data.Models.Product.ShippingInfo() { Country = s.Country, Price = s.Price, Service = s.Service }).ToList();
            p.EnergyEfficiencyClass = new Data.Models.Product.EnergyEfficiency()
            {
                EnergyEfficiencyClass = feed.energy_efficiency_class,
                MaxEnergyEfficiencyClass = feed.max_energy_efficiency_class,
                MinEnergyEfficiencyClass = feed.min_energy_efficiency_class
            };
            p.CustomLabel = new Data.Models.Product.CustomLabels()
            {
                CustomLabel0 = feed.custom_label_0,
                CustomLabel1 = feed.custom_label_1,
                CustomLabel2 = feed.custom_label_2,
                CustomLabel3 = feed.custom_label_3,
                CustomLabel4 = feed.custom_label_4,
            };
            p.ShippingLabel = feed.shipping_label;
            p.Size = feed.size;
            p.SizeSystem = feed.size_system;
            p.SizeType = feed.size_type;
            p.SubscriptionCost = feed.subscription_cost;
            var taxInfos = new List<Data.Models.Product.Tax>();
            if (feed.tax != null && feed.tax.Count > 0)
            {
                foreach (var tax in feed.tax)
                {
                    taxInfos.Add(new Data.Models.Product.Tax()
                    {
                        country = tax.country,
                        rate = tax.rate,
                        tax_ship = tax.tax_ship,
                        region = tax.region
                    });
                }
            }
            p.TaxInfo = taxInfos;
            p.TaxCategory = feed.tax_category;
            p.Title = feed.title;
            p.TransitTimeLabel = feed.transit_time_label;
            p.UnitPricingBaseMeasure = feed.unit_pricing_base_measure;
            p.UnitPricingMeasure = feed.unit_pricing_measure;
            return p;
        }

        public static ProductListModel ToResponseModel(Data.Models.Product p)
        {
            ProductListModel responseModel = new ProductListModel();
            responseModel.products = new List<Models.Products.Product>();
            Models.Products.Product selectedProduct = new Models.Products.Product();
            responseModel.filters = null;
            responseModel.categories = null;
            responseModel.pageNumber = 1;
            responseModel.pageSize = 10;
            responseModel.sortList = null;
            responseModel.totalResultCount = 1;
            responseModel.products = new List<Models.Products.Product>();
            selectedProduct.id = p.Id;
            selectedProduct.productName = p.Title;
            selectedProduct.headline = p.Gtin;
            selectedProduct.inStock = p.Availability == "in stock";
            selectedProduct.listPrice = new Models.Products.Price();
            selectedProduct.listPrice.amount = p.PriceInfo.Amount;
            selectedProduct.listPrice.currency = p.PriceInfo.Currency;
            selectedProduct.listPrice.amountDefaultCurrency = p.PriceInfo.Amount.ToString();
            selectedProduct.picture = p.ImageLink;
            selectedProduct.pictures = new List<Picture>();
            foreach (var pic in p.AdditionalImageLink)
            {
                selectedProduct.pictures.Add(new Picture() { url = pic });
            }
            StringBuilder shipmentInformationBuilder = new StringBuilder();
            foreach (var shipmentInfo in p.Shipping)
            {
                shipmentInformationBuilder.Append(shipmentInfo.Service);
                shipmentInformationBuilder.Append("-");
                shipmentInformationBuilder.Append(shipmentInfo.Price);
                shipmentInformationBuilder.Append("-");
                shipmentInformationBuilder.Append(shipmentInfo.Country);
                shipmentInformationBuilder.Append("/");
            }
            selectedProduct.shipmentInformation = shipmentInformationBuilder.ToString();
            selectedProduct.actions = null;
            selectedProduct.additionaltexts = new List<AdditionalText>() { new AdditionalText() { key = "", text = p.Description } };
            selectedProduct.backInStockSubSelectedVariant = null;
            selectedProduct.constraint = null;
            selectedProduct.creditCardInstallments = null;
            selectedProduct.customInputs = null;
            selectedProduct.isFavorite = false;
            selectedProduct.productDetailUrl = p.Link;
            selectedProduct.productUrl = p.Link;
            selectedProduct.hasVariant = p.ProductVariants?.Count() > 0;
            selectedProduct.variants = new List<Group>();
            if (p.ProductVariants != null)
            {
                foreach (var variant in p.ProductVariants)
                {
                    Group variantGroup = new Group() { groupId = variant.Id, groupName = variant.ItemGroupId };
                    variantGroup.features = new List<GroupFeature>();
                    variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.Id), value = variant.Id });
                    variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.Color), value = variant.Color });
                    variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.ImageLink), value = variant.ImageLink });
                    variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.ItemGroupId), value = variant.ItemGroupId });
                    variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.Mpn), value = variant.Mpn });
                    variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.AdditionalImageLink), value = string.Join(',', variant.AdditionalImageLink) });

                    foreach (var variantCombinations in variant.VariantCombinations)
                    {
                        variantGroup.features.Add(new GroupFeature()
                        {
                            displayName = "VariantCombinations",
                            value = nameof(variantCombinations.Id) + '=' + variantCombinations.Id + ',' + nameof(variantCombinations.Size) + '=' +
                            variantCombinations.Size + ',' + nameof(variantCombinations.Gtin) + '=' + variantCombinations.Gtin
                        });
                    }
                    selectedProduct.variants.Add(variantGroup);
                }
            }
            selectedProduct.strikeoutPrice = new Models.Products.Price();
            selectedProduct.strikeoutPrice.amount = p.PriceInfo?.Amount ?? 0;
            selectedProduct.strikeoutPrice.amountDefaultCurrency = p.PriceInfo?.Amount.ToString();
            selectedProduct.strikeoutPrice.currency = p.PriceInfo?.Currency;
            selectedProduct.subscription = null;
            selectedProduct.unit = null;
            selectedProduct.features = null;
            responseModel.products.Add(selectedProduct);
            return responseModel;
        }

        public static ProductListModel ToResponseModelList(List<Data.Models.Product> productList)
        {
            ProductListModel responseModel = new ProductListModel();
            responseModel.products = new List<Models.Products.Product>();

            responseModel.filters = null;
            responseModel.categories = null;
            responseModel.sortList = null;
            responseModel.totalResultCount = productList.Count;
            responseModel.products = new List<Models.Products.Product>();
            foreach (var p in productList)
            {
                Models.Products.Product selectedProduct = new Models.Products.Product();
                selectedProduct.id = p.Id;
                selectedProduct.productName = p.Title;
                selectedProduct.headline = p.Gtin;
                selectedProduct.inStock = p.Availability == "in stock";

                selectedProduct.listPrice = new Models.Products.Price();
                selectedProduct.listPrice.amount = p.PriceInfo?.Amount ?? 0;
                selectedProduct.listPrice.currency = p.PriceInfo?.Currency;
                selectedProduct.listPrice.amountDefaultCurrency = p.PriceInfo?.Amount.ToString();

                selectedProduct.strikeoutPrice = new Models.Products.Price();
                selectedProduct.strikeoutPrice.amount = p.SalePriceInfo?.Amount ?? 0;
                selectedProduct.strikeoutPrice.amountDefaultCurrency = p.SalePriceInfo?.Currency;
                selectedProduct.strikeoutPrice.currency = p.SalePriceInfo?.Amount.ToString();

                selectedProduct.picture = p.ImageLink;
                selectedProduct.pictures = new List<Picture>();
                foreach (var pic in p.AdditionalImageLink)
                {
                    selectedProduct.pictures.Add(new Picture() { url = pic });
                }
                StringBuilder shipmentInformationBuilder = new StringBuilder();
                foreach (var shipmentInfo in p.Shipping)
                {
                    shipmentInformationBuilder.Append(shipmentInfo.Service);
                    shipmentInformationBuilder.Append("-");
                    shipmentInformationBuilder.Append(shipmentInfo.Price);
                    shipmentInformationBuilder.Append("-");
                    shipmentInformationBuilder.Append(shipmentInfo.Country);
                    shipmentInformationBuilder.Append("/");
                }
                selectedProduct.shipmentInformation = shipmentInformationBuilder.ToString();
                selectedProduct.actions = null;
                selectedProduct.additionaltexts = new List<AdditionalText>() { new AdditionalText() { key = "", text = p.Description } };
                selectedProduct.backInStockSubSelectedVariant = null;
                selectedProduct.constraint = null;
                selectedProduct.creditCardInstallments = null;
                selectedProduct.customInputs = null;
                selectedProduct.isFavorite = false;
                selectedProduct.productDetailUrl = p.Link;
                selectedProduct.productUrl = p.Link;
                selectedProduct.hasVariant = p.ProductVariants?.Count() > 0;
                selectedProduct.variants = new List<Group>();
                if (p.ProductVariants != null)
                {
                    foreach (var variant in p.ProductVariants)
                    {
                        Group variantGroup = new Group() { groupId = variant.Id, groupName = variant.ItemGroupId };
                        variantGroup.features = new List<GroupFeature>();
                        variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.Id), value = variant.Id });
                        variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.Color), value = variant.Color });
                        variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.ImageLink), value = variant.ImageLink });
                        variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.ItemGroupId), value = variant.ItemGroupId });
                        variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.Mpn), value = variant.Mpn });
                        variantGroup.features.Add(new GroupFeature() { displayName = nameof(variant.AdditionalImageLink), value = string.Join(',', variant.AdditionalImageLink) });
                        variantGroup.features.Add(new GroupFeature() { displayName = "VariantCombinationSet", value = string.Join(',', variant.AdditionalImageLink) });
                        GroupFeature variantCombinationsFeature = new GroupFeature();
                        variantCombinationsFeature.VariantCombinationSet = new List<GroupFeature>();
                        foreach (var variantCombinations in variant.VariantCombinations)
                        {
                            variantCombinationsFeature.VariantCombinationSet.Add(new GroupFeature() {displayName = nameof(variantCombinations.Id), value = variantCombinations.Id });
                            variantCombinationsFeature.VariantCombinationSet.Add(new GroupFeature() { displayName = nameof(variantCombinations.Gtin), value = variantCombinations.Gtin });
                            variantCombinationsFeature.VariantCombinationSet.Add(new GroupFeature() { displayName = nameof(variantCombinations.Size), value = variantCombinations.Size });                            
                        }
                        selectedProduct.variants.Add(variantGroup);
                    }
                }
                selectedProduct.subscription = null;
                selectedProduct.unit = null;
                selectedProduct.features = null;
                responseModel.products.Add(selectedProduct);
            }
            return responseModel;
        }

    }
}
