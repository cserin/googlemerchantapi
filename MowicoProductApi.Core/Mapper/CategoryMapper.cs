﻿using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Mapper
{
    public class CategoryMapper
    {
        public static Category FromDto(CategoryDTO dto)
        {
            Category c = new Category();
            c.Id = dto.Id;
            c.Name = dto.Name;
            c.ParentCategoryId = dto.ParentCategoryId;
            c.IsEnabled = dto.IsEnabled;
            return c;
        }

        public static CategoryDTO ToDto(Category category)
        {
            CategoryDTO dto = new CategoryDTO();
            dto.Id = category.Id;
            dto.Name = category.Name;
            dto.ParentCategoryId = category.ParentCategoryId;
            dto.IsEnabled = category.IsEnabled;
            return dto;
        }
    }
}
