﻿using MowicoProductApi.Core.Models;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Core.Mapper
{
    public class MerchantMapper
    {

        public static MerchantDTO ToDto(MowicoMerchant merchant)
        {
            return new MerchantDTO()
            {
                CreatedAt = merchant.CreatedAt,
                Id = merchant.Id,
                IsEnabled = merchant.IsActive,
                Name = merchant.Name,
                IsTaxonomyParsed = merchant.IsTaxonomyParsed,
                IsProductListParsed = merchant.IsProductListParsed,
                HasCustomTaxonomy = merchant.HasCustomTaxonomy,
                AreProductsMapped = merchant.AreProductsMapped
            };
        }

        public static MerchantModel ToModel(MowicoMerchant merchant)
        {
            return new MerchantModel()
            {
                CreatedAt = merchant.CreatedAt,
                MerchantId = merchant.Id,
                Id = merchant.Id,
                IsEnabled = merchant.IsActive,
                Name = merchant.Name,
                IsTaxonomyParsed = merchant.IsTaxonomyParsed,
                IsProductListParsed = merchant.IsProductListParsed,
                HasCustomTaxonomy = merchant.HasCustomTaxonomy,
                AreProductsMapped = merchant.AreProductsMapped
            };
        }

        public static MowicoMerchant FromModel(MerchantModel merchant)
        {
            return new MowicoMerchant()
            {
                Id = merchant.Id,
                IsActive = merchant.IsEnabled,
                Name = merchant.Name,
                IsTaxonomyParsed = merchant.IsTaxonomyParsed,
                IsProductListParsed = merchant.IsProductListParsed,
                HasCustomTaxonomy = merchant.HasCustomTaxonomy,
                AreProductsMapped = merchant.AreProductsMapped
            };
        }

        public static MowicoMerchant FromDto(MerchantDTO dto)
        {
            return new MowicoMerchant()
            {
                Name = dto.Name,
                Id = dto.Id,
                IsActive = dto.IsEnabled,
                AreProductsMapped = dto.AreProductsMapped,
                HasCustomTaxonomy = dto.HasCustomTaxonomy,
                IsProductListParsed = dto.IsProductListParsed,
                IsTaxonomyParsed = dto.IsTaxonomyParsed
            };
        }
    }
}
