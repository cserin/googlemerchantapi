﻿using ExcelDataReader;
using MongoDB.Bson;
using MowicoProductApi.Business.Models;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace MowicoProductApi.Core.Parser
{
    public class CategoryParser
    {
        public static List<CategoryDTO> ParseCategoriesWithName(List<Product> productList)
        {
            //var fileName = @"C:\Users\TMOB L119\Desktop\MowicoMerchant\taxonomy-with-ids.tr-TR.xls";
            //DataSet categoryDs;

            List<CategoryDTO> categories = new List<CategoryDTO>();
            foreach (var product in productList)
            {
                var productCategories = product.GoogleProductCategory.Split('>').ToList();
                for (int i = 0; i < productCategories.Count(); i++)
                {
                    //category does not exist, create new one
                    if (!categories.Any(c => c.Name == productCategories[i].Trim()))
                    {
                        string parentCategoryName = (i == 0) ? null : productCategories[i - 1]?.Trim().ToString();
                        CategoryDTO categoryDto = new CategoryDTO()
                        {
                            Id = ObjectId.GenerateNewId().ToString(),
                            Name = productCategories[i].Trim().ToString(),
                            IsEnabled = true,
                            ParentCategoryId = (string.IsNullOrEmpty(parentCategoryName)) ? null : categories.FirstOrDefault(c => c.Name == parentCategoryName)?.Id
                        };
                        categories.Add(categoryDto);
                    }
                }
            }
            return categories;
        }

        public static Tuple<List<CategoryDTO>, List<Product>> ParseCategoriesWithId(List<Product> productList)
        {
            var fileName = Constants.Constants.GoogleMerchantCategoryExcelFilePath;
            DataSet categoryDs;

            //this is required for .net core projects
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            try
            {
                using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        //system.text.encoding.codepages extension
                        categoryDs = reader.AsDataSet();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (categoryDs?.Tables[0] != null)
            {
                try
                {
                    DataRow[] rows = categoryDs?.Tables[0].Select();
                    List<CategoryDTO> categoryList = new List<CategoryDTO>();
                    var replacedProductList = productList;
                    foreach (var product in productList)
                    {
                        var productCategories = product.GoogleProductCategory.Split('>').ToList();
                        // "155 > 154 > 153"
                        //productCategories = new List<string>() {"10","110","1" };
                        List<string> categoryNameList = new List<string>();
                        for (int i = 0; i < productCategories.Count(); i++)
                        {
                            var correspondingCategoryName = rows.Where(r => Convert.ToInt32(r["Column0"]) == Convert.ToInt32(productCategories[i]))?.FirstOrDefault()?.ItemArray?.Last(r => !string.IsNullOrEmpty(r.ToString()));
                            if (correspondingCategoryName != null)
                            {
                                categoryNameList.Add(correspondingCategoryName.ToString());
                            }
                        }
                        replacedProductList.Where(p => p.Id == product.Id).FirstOrDefault().GoogleProductCategory = string.Join('>', categoryNameList);
                        replacedProductList.Where(p => p.Id == product.Id).FirstOrDefault().ProductCategoryList = productCategories;
                    }
                    List<CategoryDTO> categories = new List<CategoryDTO>();
                    foreach (var product in replacedProductList)
                    {
                        var productCategories = product.GoogleProductCategory.Split('>').ToList();
                        for (int i = 0; i < productCategories.Count(); i++)
                        {
                            //category does not exist, create new one
                            if (!categories.Any(c => c.Name == productCategories[i]))
                            {
                                string parentCategoryName = (i == 0) ? null : productCategories[i - 1]?.ToString();
                                CategoryDTO categoryDto = new CategoryDTO()
                                {
                                    Id = ObjectId.GenerateNewId().ToString(),
                                    Name = productCategories[i].ToString(),
                                    IsEnabled = true,
                                    ParentCategoryId = (string.IsNullOrEmpty(parentCategoryName)) ? null : categories.FirstOrDefault(c => c.Name == parentCategoryName).Id
                                };
                                categories.Add(categoryDto);
                            }
                        }
                    }
                    return new Tuple<List<CategoryDTO>, List<Product>>(categories, replacedProductList);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return null;
        }

        public static List<Business.Models.Category> BuildTreeAndReturnRootNodes(List<Business.Models.Category> flatItems)
        {
            var byIdLookup = flatItems.ToLookup(i => i.Id);
            foreach (var item in flatItems)
            {
                if (item.ParentCategoryId != null)
                {
                    var parent = byIdLookup[item.ParentCategoryId].First();
                    parent.Children = new List<Business.Models.Category>();
                    parent.Children.Add(item);
                }
            }
            return flatItems.Where(i => i.ParentCategoryId == null).ToList();
        }

    }
}
