﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using static MowicoProductApi.Data.Models.Product;

namespace MowicoProductApi.Core.Parser
{
    public static class ProductParser
    {

        public static List<ProductDtoRss.ChannelFeed.ItemFeed> ConvertXML(string productXml)
        {
            //string xmlContent = "";
            //using (WebClient client = new WebClient())
            //{
            //    xmlContent = client.DownloadString(productXml);
            //}
            if (string.IsNullOrEmpty(productXml))
            {
                throw new Exception("Xml content is empty");
            }
            //remove linebreaks
            productXml = Regex.Replace(productXml, @"\t|\n|\r", "");
            //test amacli farkli xmller kullanildi. Kaldirilacak
            //var filepath = @"C:\Users\TMOB L119\Desktop\MowicoMerchant\example_feed_xml_rss.xml";
            //string googleXml = File.ReadAllText(filepath);
            try
            {
                var parsedProductList = Deserialize<ProductDtoRss>(productXml);
                //var parsedProductList = Deserialize<ProductDtoRss>(productXml);
                return parsedProductList.Channel.Item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ProductDtoRssNoNamespace.ChannelFeed.ItemFeed> ConvertXMLNoNamespace(string productXml)
        {
            //string xmlContent = "";
            //using (WebClient client = new WebClient())
            //{
            //    xmlContent = client.DownloadString(productXml);
            //}
            
            if (string.IsNullOrEmpty(productXml))
            {
                throw new Exception("Xml content is empty");
            }
            //string googleXml = File.ReadAllText(productXml);
            //var filepath = @"C:\Users\TMOB L119\Desktop\MerchantXML\ApparelExampleXml.xml";
            //productXml = File.ReadAllText(googleXml);
            try
            {
                var parsedProductList = Deserialize<ProductDtoRssNoNamespace>(productXml);
                return parsedProductList.Channel.Item;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        static T Deserialize<T>(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlReaderSettings settings = new XmlReaderSettings();
            using (StringReader textReader = new StringReader(xml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }

        public static Tuple<string,bool> HasNameSpacePrefix(string xmlFeed)
        {
            string xmlContent = "";
            using (WebClient client = new WebClient())
            {
                xmlContent = client.DownloadString(xmlFeed);
            }
            //xmlContent = File.ReadAllText(@"C:\Users\TMOB L119\Desktop\MerchantXML\IstanbulBilisimProductWithNs.xml");            
            if (xmlContent.Contains("<g:title>"))
            {
                return new Tuple<string,bool>(xmlContent,true);
            }
            return new Tuple<string, bool>(xmlContent, false);
        }

        public static List<Data.Models.Product> ResolveProductVariants(List<Data.Models.Product> products)
        {
            var productsWithVariants = products;
            var itemsWithVariants = products.Where(p => !string.IsNullOrEmpty(p.ItemGroupId));
            var itemsGroupedByVariants = itemsWithVariants?.GroupBy(p => p.ItemGroupId);
            if (itemsGroupedByVariants != null && itemsGroupedByVariants.Count() > 0)
            {
                foreach (var itemGroups in itemsGroupedByVariants)
                {
                    var mainProduct = itemGroups.FirstOrDefault();
                    
                    var productVariants = new List<Data.Models.Product.Variants>();
                    //first product is the main product, rest are variants. remove them and add them to main product's variant list.
                    foreach (var item in itemGroups)
                    {
                        mainProduct.ProductVariants = new List<Data.Models.Product.Variants>();
                        var variantsGroupedByColor = itemGroups.GroupBy(i => i.Color);
                        var variant = new Data.Models.Product.Variants();
                        foreach (var itemsByColor in variantsGroupedByColor)
                        {
                            variant.Color = itemsByColor.Key.ToString();
                            variant.ItemGroupId = itemsByColor.FirstOrDefault()?.ItemGroupId;
                            variant.Id = ObjectId.GenerateNewId().ToString();
                            variant.Mpn = itemsByColor.FirstOrDefault()?.Mpn;
                            variant.ImageLink = itemsByColor.FirstOrDefault()?.ImageLink;
                            variant.AdditionalImageLink = itemsByColor.FirstOrDefault()?.AdditionalImageLink;
                            variant.VariantCombinations = new List<VariantCombination>();
                            foreach (var colorItems in itemsByColor)
                            {
                                VariantCombination combination = new VariantCombination();
                                combination.Id = ObjectId.GenerateNewId().ToString();
                                combination.Size = colorItems.Size;
                                combination.Gtin = colorItems.Gtin;
                                variant.VariantCombinations.Add(combination);
                                productsWithVariants.Remove(colorItems);
                            }
                            var variantTemp = new Variants(variant);
                            mainProduct.ProductVariants.Add(variantTemp);
                        }
                    }
                    productsWithVariants.Add(mainProduct);
                }
            }
            return productsWithVariants;

        }
    }
}
