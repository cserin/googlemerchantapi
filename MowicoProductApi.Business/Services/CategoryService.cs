﻿using MongoDB.Bson;
using MongoDB.Driver;
using MowicoCategoryApi.Data.Repository;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Business.Models;
using MowicoProductApi.Core;
using MowicoProductApi.Core.BackupManager;
using MowicoProductApi.Core.Constants;
using MowicoProductApi.Core.Helpers;
using MowicoProductApi.Core.Mapper;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Category;
using MowicoProductApi.Core.Parser;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MowicoProductApi.Business.Services
{
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository _categoryRepository;
        private IProductRepository _productRepository;
        private IMerchantRepository _merchantRepository;

        public CategoryService(ICategoryRepository categoryRepository, IProductRepository productRepository, IMerchantRepository merchantRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _merchantRepository = merchantRepository;
        }

        public Response<CategoryResponseModel> GetCategories()
        {
            try
            {
                var categoryList = _categoryRepository.AsQueryable().Where(c => c.IsEnabled).ToList();
                CategoryResponseModel responseModel = new CategoryResponseModel();
                responseModel.Categories = new List<Business.Models.Category>();
                foreach (var category in categoryList)
                {
                    Business.Models.Category categoryResp = new Business.Models.Category();
                    categoryResp.Id = category.Id;
                    categoryResp.Name = category.Name;
                    categoryResp.ParentCategoryId = category.ParentCategoryId;
                    categoryResp.IsRoot = (category.ParentCategoryId == null) ? true : false;
                    categoryResp.IsLeaf = (categoryList.Any(c => c.ParentCategoryId == category.Id)) ? false : true;
                    responseModel.Categories.Add(categoryResp);
                }
                var categoryTreeResponse = CategoryParser.BuildTreeAndReturnRootNodes(responseModel.Categories);
                responseModel.Categories = categoryTreeResponse;
                return new Response<CategoryResponseModel>(responseModel);
            }
            catch (Exception ex)
            {
                return new Response<CategoryResponseModel>() { Succeeded = false, Message = "Error while retrieving categories.", Errors = new List<string>() { ex.Message } };
            }

        }

        public Response<CategoryParseResponseModel> ParseCategories(string merchantId)
        {
            try
            {
                ObjectId objId = new ObjectId();
                if (!ObjectId.TryParse(merchantId, out objId))
                {
                    return new Response<CategoryParseResponseModel>() { Succeeded = false, Message = "Invalid Merchant Id." };
                }
                //check if merchant exists
                MowicoMerchant merchant = _merchantRepository.FindOne(m => m.Id == merchantId && m.IsActive);
                if (merchant == null)
                {
                    return new Response<CategoryParseResponseModel>() { Succeeded = false, Message = "Merchant not found or not active." };
                }
                if (_categoryRepository.DatabaseFound(merchantId))
                {
                    var client = _categoryRepository.GetMongoClient();
                    var merchantDB = client.GetDatabase(merchantId);
                    string categoryExportPath = GeneralHelper.GetCollectionExportPath(Constants.CategoryCollectionName, merchantId);
                    //if there are any categories, export the db, drop it afterwards
                    try
                    {
                        BackupManager.WriteCollectionToFile(merchantDB, Constants.CategoryCollectionName, categoryExportPath).Wait();
                    }
                    catch (Exception ex)
                    {
                        return new Response<CategoryParseResponseModel>() { Succeeded = false, Message = "Category collection backup error.", Errors = new List<string>() { ex.Message } };
                    }
                    if (merchantDB.GetCollection<Data.Models.Category>(Constants.CategoryCollectionName).AsQueryable().Count() > 0)
                    {
                        merchantDB.DropCollection(Constants.CategoryCollectionName);
                    }
                }
                var productList = _productRepository.AsQueryable().ToList();
                //category tree might be sent with IDs. In this case, we need to map google categories to Ids.
                var categoryHasNames = StringValueChecker.HasStringValues(productList.FirstOrDefault().GoogleProductCategory);
                List<CategoryDTO> categoryDtoList = new List<CategoryDTO>();
                if (categoryHasNames)
                {
                    categoryDtoList = CategoryParser.ParseCategoriesWithName(productList);
                }
                else
                {
                    Tuple<List<CategoryDTO>, List<Product>> categoryAndProductList = CategoryParser.ParseCategoriesWithId(productList);
                    categoryDtoList = categoryAndProductList.Item1;
                    //We replaced ids with names, so we need to update every value.
                    var productUpdateList = categoryAndProductList.Item2;
                    foreach (var updatedProduct in productUpdateList)
                    {
                        _productRepository.ReplaceOne(updatedProduct);
                    }
                }
                if (categoryDtoList == null)
                {
                    return new Response<CategoryParseResponseModel>() { Succeeded = false, Message = "Error occured while parsing categories." };
                }
                List<Data.Models.Category> categoryList = new List<Data.Models.Category>();
                foreach (var categoryDto in categoryDtoList)
                {
                    categoryList.Add(CategoryMapper.FromDto(categoryDto));
                }
                if (categoryList.Count > 0)
                {
                    _categoryRepository.InsertMany(categoryList);
                    merchant.IsTaxonomyParsed = true;
                    _merchantRepository.ReplaceOne(merchant);
                }
                return new Response<CategoryParseResponseModel>() { Succeeded = true, Data = new CategoryParseResponseModel() { InsertedCategoriesCount = categoryList.Count } };
            }
            catch (Exception ex)
            {
                return new Response<CategoryParseResponseModel>() { Succeeded = false, Message = "Category parse error.", Errors = new List<string>() { ex.Message } };
            }
            //check if merchant db exists.

        }

        public void SetCollectionNames(string languageId)
        {
            //Turkish
            switch (languageId)
            {
                case "1":
                    _categoryRepository.CollectionNames = new CollectionNames() { CategoryCollection = Constants.CategoryCollectionName + Constants.TurkishSuffix, ProductCollection = Constants.ProductCollectionName + Constants.TurkishSuffix };
                    break;
                case "2":
                    _categoryRepository.CollectionNames = new CollectionNames() { CategoryCollection = Constants.CategoryCollectionName + Constants.EnglishSuffix, ProductCollection = Constants.ProductCollectionName + Constants.EnglishSuffix }; ;
                    break;
                default:
                    break;
            }
        }

        public void SetMerchantId(string merchantId)
        {
            _categoryRepository.MerchantId = merchantId;
        }
    }
}
