﻿using MowicoCategoryApi.Data.Repository;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core.Models;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MowicoProductApi.Business.Services
{
    public class ProductCategoryMapService : IProductCategoryMapService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IProductRepository _productRepository;

        public ProductCategoryMapService(ICategoryRepository categoryRepository, IProductRepository productRepository)
        {
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
        }

        public Response<ProductCategoryMapResponseModel> MapProductsToCategories(string merchantId)
        {
            try
            {
                var products = _productRepository.AsQueryable();
                var categories = _categoryRepository.AsQueryable();
                var updatedProductList = Core.Mapper.ProductCategoryMapper.MapProductsToCategories(products, categories);
                //if no errors, update one field of document, ProductCategoryList
                foreach (var product in updatedProductList)
                {
                    _productRepository.ReplaceOne(product);
                }
                return new Response<ProductCategoryMapResponseModel>(new ProductCategoryMapResponseModel() { NumberOfCategoriesMapped = categories.Count(), NumberOfProductsMapped = updatedProductList.Count });
            }
            catch (Exception ex)
            {
                return new Response<ProductCategoryMapResponseModel>() { Succeeded = false, Message = "Error while mapping products to categories", Errors = new List<string>() { ex.Message } };
            }
        }
    }
}
