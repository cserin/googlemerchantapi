﻿using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core;
using MowicoProductApi.Core.Mapper;
using MowicoProductApi.Core.Models;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Data.Repository;
using MowicoProductApi.Data.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MowicoProductApi.Business.Services
{
    public class MerchantService : IMerchantService
    {
        private readonly IMerchantRepository _merchantRepository;

        public MerchantService(IMerchantRepository merchantRepository)
        {

            _merchantRepository = merchantRepository;
        }

        public async void DeleteMerchantAsync(string merchantId)
        {
            try
            {
                await _merchantRepository.DeleteOneAsync(m => m.Id == merchantId && m.IsActive);
            }
            catch (System.Exception ex)
            {
                //delete failed, add log and exception handler
                throw ex;
            }
        }

        public MerchantModel GetMerchantByIdAsync(string merchantId)
        {
            try
            {
                var merchant = _merchantRepository.FindById(merchantId);
                return MerchantMapper.ToModel(merchant);
            }
            catch (System.Exception ex)
            {
                return new MerchantModel() { ErrorCode = 1, ErrorMessage = "No valid merchant found", ErrorDetails = ex.Message };
            }
        }

        public List<MerchantModel> GetMerchants()
        {
            try
            {
                return _merchantRepository.AsQueryable().ToList().Select(m => MerchantMapper.ToModel(m)).ToList();
            }
            catch (System.Exception ex)
            {
                //Log Error
                return new List<MerchantModel>() { new MerchantModel() { ErrorCode = 2, ErrorMessage = "Could not retrieve merchants.", ErrorDetails = ex.Message } };
            }
        }

        public MerchantModel InsertNewMerchantAsync(MerchantModel merchant)
        {
            try
            {
                if (_merchantRepository.FindOne(c => c.Name == merchant.Name) == null)
                {
                    _merchantRepository.InsertOne(MerchantMapper.FromModel(merchant));
                    var addedMerchant = _merchantRepository.FindOne(c => c.Name == merchant.Name);
                    return MerchantMapper.ToModel(addedMerchant);
                }
                else
                {
                    return new MerchantModel() { ErrorCode = 3, ErrorMessage = "Merchant with specified name already exists.", ErrorDetails = "" };
                    //merchant with given name exists, add log and exception handler
                }
            }
            catch (System.Exception ex)
            {
                return new MerchantModel() { ErrorCode = 4, ErrorMessage = "Could not insert merchant.", ErrorDetails = ex.Message };
                //insert fail, add log and exception handler
            }
        }

        public void SetMerchantId(string merchantId)
        {
            _merchantRepository.MerchantId = merchantId;
        }

        public MerchantModel UpdateMerchantAsync(MerchantModel merchantModel)
        {
            try
            {
                if (_merchantRepository.FindOne(m => m.Id == merchantModel.Id) != null)
                {
                    _merchantRepository.ReplaceOne(MerchantMapper.FromModel(merchantModel));
                    return MerchantMapper.ToModel(_merchantRepository.FindOne(m => m.Id == merchantModel.Id));
                }
                return new MerchantModel() { ErrorCode = 5, ErrorMessage = "Merchant not found.", ErrorDetails = "" };
            }
            catch (System.Exception ex)
            {
                return new MerchantModel() { ErrorCode = 6, ErrorMessage = "Could not update merchant.", ErrorDetails = ex.Message };
                //update fail, add log and exception handler
            }
        }
    }
}
