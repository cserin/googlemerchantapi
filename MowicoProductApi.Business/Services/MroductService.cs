﻿using MongoDB.Driver;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Business.Services
{
    public class MroductService : MroductRepository, IMroductService
    {
        public MroductService(IMongoCollection<Product> collection, IMongoConnection connection) :base(collection, connection)
        {
            
        }

        public List<Product> GetProducts()
        {
            return _collection.AsQueryable().ToList();
        }

        public List<Product> GetTopSellingProducts()
        {
            throw new NotImplementedException();
        }
    }
}
