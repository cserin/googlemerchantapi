﻿using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver;
using MowicoCategoryApi.Data.Repository;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core;
using MowicoProductApi.Core.BackupManager;
using MowicoProductApi.Core.Constants;
using MowicoProductApi.Core.Helpers;
using MowicoProductApi.Core.Mapper;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Product.RequestModel;
using MowicoProductApi.Core.Models.Product.ResponseModel;
using MowicoProductApi.Core.Models.Products;
using MowicoProductApi.Core.Parser;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MowicoProductApi.Business.Services
{
    public class ProductService : IProductService

    {
        private readonly IProductRepository _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMerchantRepository _merchantRepository;

        public ProductService(IProductRepository productRepository, ICategoryRepository categoryRepository, IMerchantRepository merchantRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _merchantRepository = merchantRepository;
        }

        public Response<ProductListModel> GetProductById(string productId)
        {
            if (!GeneralHelper.CheckObjectId(productId))
            {
                return new Response<ProductListModel>() { Succeeded = false, Message = "Invalid product id." };
            }
            var product = _productRepository.FindById(productId);
            if (product == null)
            {
                return new Response<ProductListModel>() { Succeeded = false, Message = "Product not found." };
            }
            try
            {
                var productModel = ProductMapper.ToResponseModel(product);
                return new Response<ProductListModel>(productModel);
            }
            catch (Exception ex)
            {
                return new Response<ProductListModel>() { Succeeded = false, Message = "Error while creating product response model.", Errors = new List<string>() { ex.Message } };
            }
        }

        public Response<ProductParseResponseModel> ParseProducts(string merchantId, string productsXml)
        {
            try
            {                
                ObjectId objId = new ObjectId();
                if (!ObjectId.TryParse(merchantId, out objId))
                {
                    return new Response<ProductParseResponseModel>() { Errors = new List<string>() { "Invalid Merchant Id." }, Succeeded = false, Message = "Invalid Merchant Id" };
                }
                //check if merchant exists
                MowicoMerchant merchant = _merchantRepository.FindOne(m => m.Id == merchantId && m.IsActive);
                if (merchant == null)
                {
                    return new Response<ProductParseResponseModel>() { Errors = new List<string>() { "Merchant not found or not active." }, Succeeded = false, Message = "Merchant not found" };
                }
                if (_productRepository.DatabaseFound(merchantId))
                {
                    var client = _productRepository.GetMongoClient();
                    var merchantDB = client.GetDatabase(merchantId);
                    //export existing collections
                    string productExportPath = GeneralHelper.GetCollectionExportPath(Constants.ProductCollectionName, merchantId);
                    try
                    {
                        BackupManager.WriteCollectionToFile(merchantDB, Constants.ProductCollectionName, productExportPath).Wait();
                    }
                    catch (Exception ex)
                    {
                        return new Response<ProductParseResponseModel>() { Errors = new List<string>() { "Error occured while creating a backup for products." }, Succeeded = false, Message = "Product collection backup error" };
                    }
                    //drop current collections
                    if (merchantDB.GetCollection<Data.Models.Product>(Constants.ProductCollectionName).AsQueryable().Count() > 0)
                    {
                        merchantDB.DropCollection(Constants.ProductCollectionName);
                    }
                }

                //bazi xmllerde title, description ve link nodelari g: prefixi ile geliyor. Dinamik olarak nesneye attribute eklememek icin kontrol edildi
                var xmlFeedType = ProductParser.HasNameSpacePrefix(productsXml);
                List<ProductDtoRss.ChannelFeed.ItemFeed> productsWithPrefix = null;
                List<ProductDtoRssNoNamespace.ChannelFeed.ItemFeed> productsWithoutPrefix = null;
                if (xmlFeedType.Item2)
                {
                    productsWithPrefix = ProductParser.ConvertXML(xmlFeedType.Item1);
                }
                else
                {
                    productsWithoutPrefix = ProductParser.ConvertXMLNoNamespace(xmlFeedType.Item1);
                }
                List<Data.Models.Product> products = new List<Data.Models.Product>();
                if (productsWithPrefix != null)
                {
                    foreach (var product in productsWithPrefix)
                    {
                        products.Add(ProductMapper.FromDto(product));
                    }
                    if (products.Count > 0)
                    {
                        _productRepository.InsertMany(products);
                    }
                }
                else
                {
                    foreach (var product in productsWithoutPrefix)
                    {
                        products.Add(ProductMapper.FromDto(product));
                    }
                    if (products.Count > 0)
                    {
                        var productsWithVariants = ProductParser.ResolveProductVariants(products);
                        _productRepository.InsertMany(productsWithVariants);
                    }
                }
                //products are parsed, set it to true
                merchant.IsProductListParsed = true;
                _merchantRepository.ReplaceOne(merchant);
                return new Response<ProductParseResponseModel>(new ProductParseResponseModel() { InsertedProductCount = products.Count });
            }
            catch (Exception ex)
            {
                return new Response<ProductParseResponseModel>() { Errors = new List<string>() { "Error occured while parsing products." }, Succeeded = false, Message = "Product parsing error" };
            }
        }

        public void SetMerchantId(string merchantId)
        {
            _productRepository.MerchantId = merchantId;
            _categoryRepository.MerchantId = merchantId;
        }

        public Response<ProductListModel> GetProductsByCategory(ProductRequestModel request)
        {
            var productsByCategory = _productRepository.AsQueryable().Where(p => p.ProductCategoryList.Contains(request.CategoryId)).ToList();
            if (request.SearchFilters != null)
            {
                if (!string.IsNullOrEmpty(request.SearchFilters.Brand))
                {
                    productsByCategory = productsByCategory.Where(p => p.Brand == request.SearchFilters.Brand).ToList();
                }
                if (!string.IsNullOrEmpty(request.SearchFilters.Color))
                {
                    productsByCategory = productsByCategory.Where(p => p.Color == request.SearchFilters.Color).ToList();
                }
                if (!string.IsNullOrEmpty(request.SearchFilters.Size))
                {
                    List<Data.Models.Product> filteredProductList = new List<Data.Models.Product>();
                    foreach (var product in productsByCategory)
                    {
                        foreach (var variants in product?.ProductVariants)
                        {
                            foreach (var combinations in variants?.VariantCombinations)
                            {
                                if (combinations?.Size == request.SearchFilters.Size)
                                {
                                    filteredProductList.Add(product);
                                }
                            }
                        }
                    }
                    productsByCategory = filteredProductList;
                }
            }
            var availableColors = productsByCategory.GroupBy(p => p.PriceInfo.Amount);
            
            var availableSizes = productsByCategory.GroupBy(p => p.Availability);
            var totalResultCount = productsByCategory.Count();
            List<KeyValuePair<string, int>> availabilityFilters = new List<KeyValuePair<string, int>>();
            //foreach (var availability in availableSizes)
            //{
            //    availabilityFilters.Add(new KeyValuePair<string, int>()
            //}

            if (totalResultCount == 0)
            {
                return new Response<ProductListModel>() { Message = "No products found for selected category.", Succeeded = false };
            }
            if (!string.IsNullOrEmpty(request.SortId))
            {
                switch (request.SortId)
                {
                    case "0":
                        productsByCategory = productsByCategory.OrderBy(s => s.PriceInfo.Amount).ToList();
                        break;
                    case "1":
                        productsByCategory = productsByCategory.OrderByDescending(s => s.PriceInfo.Amount).ToList();
                        break;
                    case "2":
                        productsByCategory = productsByCategory.OrderBy(s => s.Title).ToList();
                        break;
                    case "3":
                        productsByCategory = productsByCategory.OrderByDescending(s => s.Title).ToList();
                        break;
                    case "4":
                        //indirim oranina gore azalan
                        productsByCategory = productsByCategory.OrderBy(s => GeneralHelper.CalculateDiscountRate((s.PriceInfo == null) ? 0 : s.PriceInfo.Amount, (s.SalePriceInfo == null) ? 0 : s.SalePriceInfo.Amount)).ToList();
                        break;
                    case "5":
                        //indirim oranina gore artan
                        productsByCategory = productsByCategory.OrderByDescending(s => GeneralHelper.CalculateDiscountRate((s.PriceInfo == null) ? 0 : s.PriceInfo.Amount, (s.SalePriceInfo == null) ? 0 : s.SalePriceInfo.Amount)).ToList();
                        break;
                    default:
                        break;
                }
            }

            ProductListModel responseModel = new ProductListModel();
            var pagedProductsByCategory = productsByCategory
                .Skip(request.PageNumber - 1 * request.PageSize)
                .Take(request.PageSize).ToList();
            responseModel = ProductMapper.ToResponseModelList(pagedProductsByCategory);
            responseModel.totalResultCount = totalResultCount;
            responseModel.pageNumber = request.PageNumber;
            responseModel.pageSize = request.PageSize;
            return new Response<ProductListModel>(responseModel);
        }

        public void SetCollectionNames(string languageId)
        {
            //Turkish
            switch (languageId)
            {
                case "1":
                    _productRepository.CollectionNames = new CollectionNames() { CategoryCollection = Constants.CategoryCollectionName + Constants.TurkishSuffix, ProductCollection = Constants.ProductCollectionName + Constants.TurkishSuffix };
                    break;
                case "2":
                    _productRepository.CollectionNames = new CollectionNames() { CategoryCollection = Constants.CategoryCollectionName + Constants.EnglishSuffix, ProductCollection = Constants.ProductCollectionName + Constants.EnglishSuffix }; ;
                    break;
                default:
                    break;
            }
        }
    }
}
