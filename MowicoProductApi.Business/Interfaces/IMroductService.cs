﻿using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Business.Interfaces
{
    public interface IMroductService : IMroductRepository
    {
        public List<Product> GetTopSellingProducts();

    }
}
