﻿using MowicoProductApi.Business.Services;
using MowicoProductApi.Core;
using MowicoProductApi.Core.Mapper;
using MowicoProductApi.Core.Models;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MowicoProductApi.Business.Interfaces
{
    public interface IMerchantService
    {
        //Task<MerchantDTO> GetMerchantByIdAsync(string merchantId);

        void SetMerchantId(string merchantId);
        List<MerchantModel> GetMerchants();
        MerchantModel GetMerchantByIdAsync(string merchantId);
        MerchantModel InsertNewMerchantAsync(MerchantModel merchantDTO);
        MerchantModel UpdateMerchantAsync(MerchantModel merchantDTO);
        void DeleteMerchantAsync(string merchantId);
    }
}
