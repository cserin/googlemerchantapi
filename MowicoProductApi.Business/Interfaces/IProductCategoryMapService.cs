﻿using MowicoProductApi.Core.Models;
using MowicoProductApi.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Business.Interfaces
{
    public interface IProductCategoryMapService 
    {
        public Response<ProductCategoryMapResponseModel> MapProductsToCategories(string merchantId);
    }
}
