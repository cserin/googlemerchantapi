﻿using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Product.RequestModel;
using MowicoProductApi.Core.Models.Product.ResponseModel;
using MowicoProductApi.Core.Models.Products;

namespace MowicoProductApi.Business.Interfaces
{
    public interface IProductService    
    {
        void SetMerchantId(string merchantId);

        void SetCollectionNames(string languageId);

        Response<ProductListModel> GetProductById(string productId);

        Response<ProductListModel> GetProductsByCategory(ProductRequestModel request);

        Response<ProductParseResponseModel>  ParseProducts(string merchantId, string productsXml);
    }
}
