﻿using MowicoProductApi.Business.Models;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Category;

namespace MowicoProductApi.Business.Interfaces
{
    public interface ICategoryService
    {        
        void SetMerchantId(string merchantId);

        void SetCollectionNames(string languageId);

        Response<CategoryResponseModel> GetCategories();

        Response<CategoryParseResponseModel> ParseCategories(string merchantId);
    }
}
