﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Business.Models
{
    public class CategoryResponseModel
    {
        public List<CategoryResponse> Categories { get; set; }
    }

    public class CategoryResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsRoot { get; set; }
        public bool IsLeaf { get; set; }
        public string ParentCategoryId { get; set; }
        public List<CategoryResponse> Children { get; set; }
    }
}
