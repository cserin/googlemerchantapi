﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MowicoCategoryTree.API.Models
{
    public class CategoryRequestModel
    {
        public Guid MerchantId { get; set; }
        public Guid CategoryId { get; set; }
    }
}
