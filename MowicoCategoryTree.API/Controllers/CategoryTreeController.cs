﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoCategoryTree.API.Models;
using MowicoProductApi.Business.Interfaces;
using Newtonsoft.Json;

namespace MowicoCategoryTree.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryTreeController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;


        public CategoryTreeController(ICategoryService categoryService, IProductService productService)
        {
            _categoryService = categoryService;
            _productService = productService;
        }

        // GET: api/CategoryTree
        [HttpGet]
        public string Get()
        {
            //get all main categories which have items
            //merchantId will be in the querystring, pass it to service.
            //var result = _categoryService.GetMainCategories();
            return JsonConvert.SerializeObject("");
        }

        // GET: api/CategoryTree/{merchantId}/{categoryId}
        [HttpGet("{merchantId}/{categoryId}", Name = "Get")]
        public string Get(string merchantId, string categoryId)
        {
            //var result = _categoryService.GetChildCategories(categoryId);
            //var sopnuc = JsonConvert.SerializeObject(result);
            //return JsonConvert.SerializeObject(result);
            return null;
        }

        // POST: api/CategoryTree
        [HttpPost]
        public void Post([FromBody] CategoryRequestModel request)
        {
            //_productService.InsertProduct(request.MerchantId.ToString());
            //var result = _categoryService.GetMainCategories(request.MerchantId, request.CategoryId);            
        }

        // PUT: api/CategoryTree/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
