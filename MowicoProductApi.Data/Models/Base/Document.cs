﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MowicoProductApi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    public abstract class Document : IDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public DateTime CreatedAt => DateTime.Now;
    }
}
