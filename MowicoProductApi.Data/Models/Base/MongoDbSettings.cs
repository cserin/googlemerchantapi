﻿using MowicoProductApi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    class MongoDbSettings : IMongoDbSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}
