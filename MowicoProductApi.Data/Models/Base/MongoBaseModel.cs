﻿using MongoDB.Bson;

namespace MowicoProductApi.Data
{
    public abstract class MongoBaseModel
    {
        public ObjectId Id { get; set; }
    }
}
