﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    public class MowicoMerchant : Document
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool HasCustomTaxonomy { get; set; }
        public bool IsProductListParsed { get; set; }
        public bool IsTaxonomyParsed { get; set; }
        public bool AreProductsMapped { get; set; }
    }
}
