﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    [BsonCollection("products")]
    public class Product : Document
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public string ImageLink { get; set; }
        public List<string> AdditionalImageLink { get; set; }
        public string MobileLink { get; set; }
        public string Availability { get; set; }
        public DateTime? AvailabilityDate { get; set; }
        public string CostsOfGoodsSold { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public Price PriceInfo { get; set; }
        public Price SalePriceInfo { get; set; }
        public SalePriceEffectiveDateInfo SalePriceEffectiveDate { get; set; }
        public string UnitPricingBaseMeasure { get; set; }
        public string UnitPricingMeasure { get; set; }
        public string Installment { get; set; }
        public string SubscriptionCost { get; set; }
        public string LoyaltyPoints { get; set; }
        public string GoogleProductCategory { get; set; }
        public string ProductType { get; set; }
        public string Brand { get; set; }
        public string Gtin { get; set; }
        public string Mpn { get; set; }
        public bool IdentifierExists { get; set; }
        public string Condition { get; set; }
        public bool Adult { get; set; }
        public int Multipack { get; set; }
        public bool IsBundle { get; set; }
        public string AgeGroup { get; set; }
        public string Color { get; set; }
        public string Gender { get; set; }
        public string Material { get; set; }
        public string Pattern { get; set; }
        public string Size { get; set; }
        public string SizeType { get; set; }
        public string SizeSystem { get; set; }
        public string ItemGroupId { get; set; }
        public string AdsRedirect { get; set; }
        public string PromotionId { get; set; }
        public string ExcludedDestination { get; set; }
        public string IncludedDestination { get; set; }
        public string ShippingLabel { get; set; }
        public string TransitTimeLabel { get; set; }
        public int MinHandlingTime { get; set; }
        public int MaxHandlingTime { get; set; }
        public List<Tax> TaxInfo { get; set; }
        public string TaxCategory { get; set; }
        public List<string> ProductCategoryList { get; set; }
        public List<ShippingInfo> Shipping { get; set; }
        public CustomLabels CustomLabel { get; set; }
        public EnergyEfficiency EnergyEfficiencyClass { get; set; }
        public List<Variants> ProductVariants { get; set; }
        public class ShippingInfo
        {
            public string Country { get; set; }
            public string Service { get; set; }
            public string Price { get; set; }

            public ShippingDimension ShippingDimensions { get; set; }

            public class ShippingDimension
            {
                public string ShippingWeight { get; set; }
                public string ShippingHeight { get; set; }
                public string ShippingWidth { get; set; }
                public string ShippingLength { get; set; }
            }

        }

        public class Price
        {
            public decimal Amount { get; set; }

            public string Currency { get; set; }
        }

        public class SalePriceEffectiveDateInfo {
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }

        public class EnergyEfficiency
        {
            public string EnergyEfficiencyClass { get; set; }
            public string MinEnergyEfficiencyClass { get; set; }
            public string MaxEnergyEfficiencyClass { get; set; }
        }

        public class CustomLabels
        {
            public string CustomLabel0 { get; set; }
            public string CustomLabel1 { get; set; }
            public string CustomLabel2 { get; set; }
            public string CustomLabel3 { get; set; }
            public string CustomLabel4 { get; set; }
        }

        public class Tax
        {
            public string country { get; set; }
            public string region { get; set; }
            public decimal? rate { get; set; }
            public string tax_ship { get; set; }
        }

        public class Variants
        {
            public Variants()
            {

            }

            public Variants(Variants v) {
                Id = v.Id;
                Color = v.Color;
                ItemGroupId = v.ItemGroupId;
                Mpn = v.Mpn;
                ImageLink = v.ImageLink;
                VariantCombinations = v.VariantCombinations;
                AdditionalImageLink = v.AdditionalImageLink;
            }

            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]
            public string Id { get; set; }
            public string Color { get; set; }
            public string ItemGroupId { get; set; }
            public string Mpn { get; set; }
            public string ImageLink { get; set; }
            public List<VariantCombination> VariantCombinations { get; set; }
            public List<string> AdditionalImageLink { get; set; }

          
        }

        public class VariantCombination
        {
            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]
            public string Id { get; set; }
            public string Size { get; set; }

            public string Gtin { get; set; }

        }
    }
}

