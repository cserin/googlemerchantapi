﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
    }
}
