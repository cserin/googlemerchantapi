﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    public class CollectionNames
    {
        public  string ProductCollection { get; set; }

        public string CategoryCollection { get; set; }
    }
}
