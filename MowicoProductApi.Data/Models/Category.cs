﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Models
{
    [BsonCollection("categories")]
    public class Category : Document
    {
        public string Name { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ParentCategoryId { get; set; }

        public bool IsEnabled { get; set; }
    }
}
