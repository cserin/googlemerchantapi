﻿using MongoDB.Driver;
using MowicoProductApi.Data.Constants;
using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MowicoProductApi.Data.Repository
{
    public class MerchantRepository : IMerchantRepository
    {
        private readonly IMongoCollection<MowicoMerchant> _merchants;
        public string MerchantId { get; set; }

        public MerchantRepository(IMongoDbSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);
            _merchants = db.GetCollection<MowicoMerchant>(settings.CollectionName);
        }

        public IQueryable<MowicoMerchant> AsQueryable()
        {
            return _merchants.AsQueryable();
        }

        public bool DatabaseFound(string databaseName)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<MowicoMerchant> FilterBy(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            return _merchants.Find(filterExpression).ToEnumerable();
        }

        public virtual IEnumerable<TProjected> FilterBy<TProjected>(Expression<Func<MowicoMerchant, bool>> filterExpression, Expression<Func<MowicoMerchant, TProjected>> projectionExpression)
        {
            return _merchants.Find(filterExpression).Project(projectionExpression).ToEnumerable();
        }

        public virtual MowicoMerchant FindOne(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            return _merchants.Find(filterExpression).FirstOrDefault();
        }

        public virtual Task<MowicoMerchant> FindOneAsync(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            return Task.Run(() => _merchants.Find(filterExpression).FirstOrDefaultAsync());
        }

        public virtual MowicoMerchant FindById(string id)
        {
            var filter = Builders<MowicoMerchant>.Filter.Eq(doc => doc.Id, id);
            return _merchants.Find(filter).SingleOrDefault();
        }

        public virtual Task<MowicoMerchant> FindByIdAsync(string id)
        {
            return Task.Run(() =>
            {
                var filter = Builders<MowicoMerchant>.Filter.Eq(doc => doc.Id, id);
                return _merchants.Find(filter).SingleOrDefaultAsync();
            });
        }

        public virtual void InsertOne(MowicoMerchant document)
        {
            _merchants.InsertOne(document);
        }

        public virtual Task InsertOneAsync(MowicoMerchant document)
        {
            return Task.Run(() => _merchants.InsertOneAsync(document));
        }

        public void InsertMany(ICollection<MowicoMerchant> documents)
        {
            _merchants.InsertMany(documents);
        }


        public virtual async Task InsertManyAsync(ICollection<MowicoMerchant> documents)
        {
            await _merchants.InsertManyAsync(documents);
        }

        public void ReplaceOne(MowicoMerchant document)
        {
            var filter = Builders<MowicoMerchant>.Filter.Eq(doc => doc.Id, document.Id);
            _merchants.FindOneAndReplace(filter, document);
        }

        public virtual async Task ReplaceOneAsync(MowicoMerchant document)
        {
            var filter = Builders<MowicoMerchant>.Filter.Eq(doc => doc.Id, document.Id);
            await _merchants.FindOneAndReplaceAsync(filter, document);
        }

        public void DeleteOne(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            _merchants.FindOneAndDelete(filterExpression);
        }

        public Task DeleteOneAsync(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            return Task.Run(() => _merchants.FindOneAndDeleteAsync(filterExpression));
        }

        public void DeleteById(string id)
        {

            var filter = Builders<MowicoMerchant>.Filter.Eq(doc => doc.Id, id);
            _merchants.FindOneAndDelete(filter);
        }

        public Task DeleteByIdAsync(string id)
        {
            return Task.Run(() =>
            {
                var filter = Builders<MowicoMerchant>.Filter.Eq(doc => doc.Id, id);
                _merchants.FindOneAndDeleteAsync(filter);
            });
        }

        public void DeleteMany(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            _merchants.DeleteMany(filterExpression);
        }

        public Task DeleteManyAsync(Expression<Func<MowicoMerchant, bool>> filterExpression)
        {
            return Task.Run(() => _merchants.DeleteManyAsync(filterExpression));
        }

        public IQueryable<MowicoMerchant> AsQueryable(string merchantId)
        {
            throw new NotImplementedException();
        }
    }
}
