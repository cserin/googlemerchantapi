﻿using MongoDB.Driver;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Repository
{
    public class MroductRepository : MongoRepository<Product,MongoDbConnection>, IMroductRepository
    {

        public MroductRepository(IMongoCollection<Product> collection, IMongoConnection connection) : base(collection,connection)
        {
        }

        public IEnumerable<Product> GetTopProducts(int number)
        {
            
            return ProductCollection.Find(c => c.Price == "100").ToList();
        }

        public IMongoCollection<Product> ProductCollection
        {
            get { return _collection as IMongoCollection<Product>; }
        }
    }
}
