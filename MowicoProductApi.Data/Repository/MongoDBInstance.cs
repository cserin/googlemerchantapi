﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Repository
{
    public sealed class MongoDBInstance
    {        
        private static volatile MongoClient instance;
        private static object syncLock = new Object();
        private static string _connectionString = "";        

        private MongoDBInstance(string connectionString)
        {
            _connectionString = connectionString;
        }

        public static MongoClient GetMongoDatabase
        {
            get
            {
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null) {
                            instance = new MongoClient(_connectionString);                            
                        }
                    }
                }
                return instance;
            }
        }
    }
}
