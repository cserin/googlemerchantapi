﻿using MongoDB.Bson;
using MongoDB.Driver;
using MowicoProductApi.Data.Constants;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MowicoProductApi.Data.Repository
{
    public class ProductRepository : IProductRepository
    {
        private IMongoClient _client { get; }

        private IMongoCollection<Product> _products;
        public string MerchantId { get; set; }
        public CollectionNames CollectionNames { get; set; }

        public ProductRepository(IMongoDbSettings settings)
        {
            _client = new MongoClient(settings.ConnectionString);
        }

        public IMongoClient GetMongoClient()
        {
            return _client;
        }

        public bool DatabaseFound(string databaseName)
        {
            using (IAsyncCursor<BsonDocument> cursor = _client.ListDatabases())
            {
                while (cursor.MoveNext())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc["name"] == databaseName)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public virtual IEnumerable<Product> FilterBy(Expression<Func<Product, bool>> filterExpression)
        {
            //where color == blue && size == m &&
            //productList
            //GroupBy(brand)
            return _products.Find(filterExpression).ToEnumerable();
        }

        public virtual IEnumerable<TProjected> FilterBy<TProjected>(Expression<Func<Product, bool>> filterExpression, Expression<Func<Product, TProjected>> projectionExpression)
        {
            return _products.Find(filterExpression).Project(projectionExpression).ToEnumerable();
        }

        public virtual Product FindOne(Expression<Func<Product, bool>> filterExpression)
        {
            return _products.Find(filterExpression).FirstOrDefault();
        }

        public virtual Task<Product> FindOneAsync(Expression<Func<Product, bool>> filterExpression)
        {
            return Task.Run(() => _products.Find(filterExpression).FirstOrDefaultAsync());
        }

        public virtual Product FindById(string id)
        {
            var filter = Builders<Product>.Filter.Eq(doc => doc.Id, id);
            return _client.GetDatabase(MerchantId).GetCollection<Product>(CollectionNames.ProductCollection).Find(filter).SingleOrDefault();
        }

        public virtual Task<Product> FindByIdAsync(string id)
        {
            return Task.Run(() =>
            {
                var filter = Builders<Product>.Filter.Eq(doc => doc.Id, id);
                return _products.Find(filter).SingleOrDefaultAsync();
            });
        }

        public virtual void InsertOne(Product document)
        {
            _products.InsertOne(document);
        }

        public virtual Task InsertOneAsync(Product document)
        {
            return Task.Run(() => _products.InsertOneAsync(document));
        }

        public void InsertMany(ICollection<Product> documents)
        {
            _client.GetDatabase(MerchantId).GetCollection<Product>(CollectionNames.ProductCollection).InsertMany(documents);
        }


        public virtual async Task InsertManyAsync(ICollection<Product> documents)
        {
            await _products.InsertManyAsync(documents);
        }

        public void ReplaceOne(Product document)
        {
            var filter = Builders<Product>.Filter.Eq(doc => doc.Id, document.Id);
            _client.GetDatabase(MerchantId).GetCollection<Product>(CollectionNames.ProductCollection).FindOneAndReplace(filter, document);
        }

        //public void UpdateMany(ICollection< Product> documents)
        //{

        //    //var filter2 = Builders<Product>.Filter.Where(x => x.Id == document.Id);
        //    //var update = Builders<Product>.Update.Set(x => x.ProductCategoryList,documents.);
        //    var result = _products.UpdateMany(filter2, update).ModifiedCount;
        //}

        public virtual async Task ReplaceOneAsync(Product document)
        {
            var filter = Builders<Product>.Filter.Eq(doc => doc.Id, document.Id);
            await _products.FindOneAndReplaceAsync(filter, document);
        }

        public void DeleteOne(Expression<Func<Product, bool>> filterExpression)
        {
            _products.FindOneAndDelete(filterExpression);
        }

        public Task DeleteOneAsync(Expression<Func<Product, bool>> filterExpression)
        {
            return Task.Run(() => _products.FindOneAndDeleteAsync(filterExpression));
        }

        public void DeleteById(string id)
        {

            var filter = Builders<Product>.Filter.Eq(doc => doc.Id, id);
            _products.FindOneAndDelete(filter);
        }

        public Task DeleteByIdAsync(string id)
        {
            return Task.Run(() =>
            {
                var filter = Builders<Product>.Filter.Eq(doc => doc.Id, id);
                _products.FindOneAndDeleteAsync(filter);
            });
        }

        public void DeleteMany(Expression<Func<Product, bool>> filterExpression)
        {
            _products.DeleteMany(filterExpression);
        }

        public Task DeleteManyAsync(Expression<Func<Product, bool>> filterExpression)
        {
            return Task.Run(() => _products.DeleteManyAsync(filterExpression));
        }

        public IMongoCollection<Product> GetProductCollection(string merchantId)
        {
            return _client.GetDatabase(merchantId).GetCollection<Product>(CollectionNames.ProductCollection);
        }

        public IQueryable<Product> AsQueryable()
        {
            return _client.GetDatabase(MerchantId).GetCollection<Product>(CollectionNames.ProductCollection).AsQueryable();
        }
    }
}
