﻿using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MowicoProductApi.Data.Repository.Interfaces
{
    public interface IMerchantRepository : IMongoRepository<MowicoMerchant>
    {
        public string MerchantId { get; set; }
        IQueryable<MowicoMerchant> AsQueryable();
    }
}
