﻿using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Repository.Interfaces
{
    public interface IMroductRepository : IMongoRepository<Product,MongoDbConnection>
    {
        //write your own bodies
        IEnumerable<Product> GetTopProducts(int number);
    }
}
