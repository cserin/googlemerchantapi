﻿using MongoDB.Driver;
using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MowicoCategoryApi.Data.Repository
{
    public interface ICategoryRepository : IMongoRepository<Category>
    {
        public string MerchantId { get; set; }

        public CollectionNames CollectionNames { get; set; }

        public IMongoClient GetMongoClient();
    }
}
