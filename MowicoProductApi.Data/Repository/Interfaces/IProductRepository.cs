﻿using MongoDB.Driver;
using MowicoProductApi.Data.Interfaces;
using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MowicoProductApi.Data.Repository
{
    public interface IProductRepository : IMongoRepository<Product>
    {
        public string MerchantId { get; set; }

        public CollectionNames CollectionNames { get; set; }
        IMongoClient GetMongoClient();
        IMongoCollection<Product> GetProductCollection(string merchantId);
    }
}
