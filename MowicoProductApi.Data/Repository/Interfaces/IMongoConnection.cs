﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Repository.Interfaces
{
    public interface IMongoConnection
    {
        string GetDatabaseName();
        
    }
}
