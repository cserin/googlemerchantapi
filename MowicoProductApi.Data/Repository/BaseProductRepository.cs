﻿using MowicoProductApi.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Repository
{
    public class BaseProductRepository : BaseMongoRepository<Product>
    {
        public BaseProductRepository(string mongoDBConnectionString, string dbName, string collectionName) : base(mongoDBConnectionString, dbName, collectionName)
        {
        }
    }
}
