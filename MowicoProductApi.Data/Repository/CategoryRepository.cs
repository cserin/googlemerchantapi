﻿using MongoDB.Bson;
using MongoDB.Driver;
using MowicoProductApi.Data.Constants;
using MowicoProductApi.Data.Models;
using MowicoProductApi.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MowicoCategoryApi.Data.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IMongoClient _client;
        private IMongoCollection<Category> _categories;
        public string MerchantId { get; set; }
        public CollectionNames CollectionNames { get; set; }

        public CategoryRepository(IMongoDbSettings settings)
        {
            _client = new MongoClient(settings.ConnectionString);
        }

        public IQueryable<Category> AsQueryable()
        {
            return _client.GetDatabase(MerchantId).GetCollection<Category>(CollectionNames.CategoryCollection).AsQueryable();
        }

        public bool DatabaseFound(string databaseName)
        {
            using (IAsyncCursor<BsonDocument> cursor = _client.ListDatabases())
            {
                while (cursor.MoveNext())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc["name"] == databaseName)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public virtual IEnumerable<Category> FilterBy(Expression<Func<Category, bool>> filterExpression)
        {
            return _categories.Find(filterExpression).ToEnumerable();
        }

        public virtual IEnumerable<TProjected> FilterBy<TProjected>(Expression<Func<Category, bool>> filterExpression, Expression<Func<Category, TProjected>> projectionExpression)
        {
            return _categories.Find(filterExpression).Project(projectionExpression).ToEnumerable();
        }

        public virtual Category FindOne(Expression<Func<Category, bool>> filterExpression)
        {
            return _categories.Find(filterExpression).FirstOrDefault();
        }

        public virtual Task<Category> FindOneAsync(Expression<Func<Category, bool>> filterExpression)
        {
            return Task.Run(() => _categories.Find(filterExpression).FirstOrDefaultAsync());
        }

        public virtual Category FindById(string id)
        {
            var filter = Builders<Category>.Filter.Eq(doc => doc.Id, id);
            return _categories.Find(filter).SingleOrDefault();
        }

        public virtual Task<Category> FindByIdAsync(string id)
        {
            return Task.Run(() =>
            {
                var filter = Builders<Category>.Filter.Eq(doc => doc.Id, id);
                return _categories.Find(filter).SingleOrDefaultAsync();
            });
        }

        public virtual void InsertOne(Category document)
        {
            _categories.InsertOne(document);
        }

        public virtual Task InsertOneAsync(Category document)
        {
            return Task.Run(() => _categories.InsertOneAsync(document));
        }

        public void InsertMany(ICollection<Category> documents)
        {
            _client.GetDatabase(MerchantId).GetCollection<Category>(CollectionNames.CategoryCollection).InsertMany(documents);
        }


        public virtual async Task InsertManyAsync(ICollection<Category> documents)
        {
            await _categories.InsertManyAsync(documents);
        }

        public void ReplaceOne(Category document)
        {
            var filter = Builders<Category>.Filter.Eq(doc => doc.Id, document.Id);
            _categories.FindOneAndReplace(filter, document);
        }

        public virtual async Task ReplaceOneAsync(Category document)
        {
            var filter = Builders<Category>.Filter.Eq(doc => doc.Id, document.Id);
            await _categories.FindOneAndReplaceAsync(filter, document);
        }

        public void DeleteOne(Expression<Func<Category, bool>> filterExpression)
        {
            _categories.FindOneAndDelete(filterExpression);
        }

        public Task DeleteOneAsync(Expression<Func<Category, bool>> filterExpression)
        {
            return Task.Run(() => _categories.FindOneAndDeleteAsync(filterExpression));
        }

        public void DeleteById(string id)
        {

            var filter = Builders<Category>.Filter.Eq(doc => doc.Id, id);
            _categories.FindOneAndDelete(filter);
        }

        public Task DeleteByIdAsync(string id)
        {
            return Task.Run(() =>
            {
                var filter = Builders<Category>.Filter.Eq(doc => doc.Id, id);
                _categories.FindOneAndDeleteAsync(filter);
            });
        }

        public void DeleteMany(Expression<Func<Category, bool>> filterExpression)
        {
            _categories.DeleteMany(filterExpression);
        }

        public Task DeleteManyAsync(Expression<Func<Category, bool>> filterExpression)
        {
            return Task.Run(() => _categories.DeleteManyAsync(filterExpression));
        }

        public IMongoClient GetMongoClient()
        {
            return _client;
        }
    }
}
