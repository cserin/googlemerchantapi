﻿using MowicoProductApi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private IMongoDbSettings _mongoDbSettings;
        private IProductRepository _productRepository;
        private ICategoryRepository _categoryRepository;

        public IProductRepository ProductRepository
        {
            get { return _productRepository = _productRepository ?? new ProductRepository(); }
        }

        public ICategoryRepository CategoryRepository { get { return _categoryRepository = _categoryRepository ?? new CategoryRepository(); } }


        public void Commit()
        {
            throw new NotImplementedException();
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }
    }
}
