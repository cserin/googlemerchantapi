﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Constants
{
    public class MongoClientInstance
    {
        
        public readonly MongoClient MongoClient;
        private string connectionString = "mongodb://localhost:27017";

        private static MongoClientInstance instance;

        public static MongoClientInstance Instance
        {
            get { return instance ?? (instance = new MongoClientInstance()); }
        }

        private MongoClientInstance()
        {
            MongoClient = new MongoClient(connectionString);
        }
    }
}
