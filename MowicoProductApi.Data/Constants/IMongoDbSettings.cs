﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MowicoProductApi.Data.Constants
{
    public interface IMongoDbSettings
    {
        string CollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
