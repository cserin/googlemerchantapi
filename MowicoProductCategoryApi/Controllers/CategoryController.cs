﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;

namespace MowicoProductCategoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController
    {
        private readonly ICategoryService categoryService;
        private readonly IProductService productService;
        private readonly IProductCategoryMapService productCategoryMapService;
        //public CategoryController(IProductService _productService, ICategoryService _categoryService)
        public CategoryController(ICategoryService _categoryService, IProductService _productService, IProductCategoryMapService _productCategoryMapService)        
        {
            categoryService = _categoryService;
            productCategoryMapService = _productCategoryMapService;
            productService = _productService;
        }

        // GET: api/Products
        [HttpGet]
        public IEnumerable<string> Get()
        {
            //productService.InsertProduct("mocal");
            //var res = categoryService.AsQueryable().ToList();
            //var res1 = productService.AsQueryable().ToList();
            productCategoryMapService.MapProductsToCategories();
            return new string[] { "value1", "value2" };
        }

        // GET: api/Products/5
        [HttpGet("{id}", Name = "Get")]
        public void Get(string merchantId, string productsXml)
        {
            //productService.InsertProduct(merchantId);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost]
        public void Post([FromBody] string body)
        {
            
        }
    }
}