﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleMerchant.API.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core.Models;
using Newtonsoft.Json;

namespace GoogleMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MerchantController : ControllerBase
    {
        private readonly IMerchantService _merchantService;

        public MerchantController(IMerchantService merchantService)
        {
            _merchantService = merchantService;
        }

        // GET: api/Merchant
        [HttpGet]
        public List<MerchantModel> Get()
        {
            return _merchantService.GetMerchants();         
        }

        // GET: api/Merchant/5
        [HttpGet("{merchantId}", Name = "Get")]
        public MerchantModel Get(string merchantId)
        {
            var merchant = _merchantService.GetMerchantByIdAsync(merchantId);
            return merchant;
        }

        // POST: api/Merchant
        [HttpPost]
        public MerchantModel Post([FromBody] MerchantModel request)
        {
            return _merchantService.InsertNewMerchantAsync(request);
        }

        // PUT: api/Merchant/5
        [HttpPut("{merchantId}")]
        public MerchantModel Put([FromBody]MerchantModel request)
        {
            return _merchantService.UpdateMerchantAsync(request);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{merchantId}")]
        public void Delete(string merchantId)
        {
            _merchantService.DeleteMerchantAsync(merchantId);
        }
    }
}
