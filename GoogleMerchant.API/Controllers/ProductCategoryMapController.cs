﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleMerchant.API.Helpers;
using GoogleMerchant.API.Models.RequestModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core.Models;
using MowicoProductApi.Core.Models.Base;

namespace GoogleMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductCategoryMapController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IProductCategoryMapService _productCategoryMapService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductCategoryMapController(ICategoryService categoryService, IProductService productService, IProductCategoryMapService productCategoryMapService, IHttpContextAccessor httpContextAccessor)
        {
            _categoryService = categoryService;
            _productService = productService;
            _productCategoryMapService = productCategoryMapService;
            _httpContextAccessor = httpContextAccessor;
            ValidateHeader.CheckHeaderAndSetMerchantInfo(_httpContextAccessor.HttpContext.Request, _categoryService, _productService);
        }

        // GET: api/ProductCategoryMap
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ProductCategoryMap/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ProductCategoryMap
        [HttpPost]
        public Response<ProductCategoryMapResponseModel> Post(ProductCategoryMapRequestModel request)
        {
            if (string.IsNullOrEmpty(request.MerchantId))
            {
                throw new NullReferenceException("MerchantId is null");
            }
            var result = _productCategoryMapService.MapProductsToCategories(request.MerchantId);
            return result;
        }

        // PUT: api/ProductCategoryMap/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
