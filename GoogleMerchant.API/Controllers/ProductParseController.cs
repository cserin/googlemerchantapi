﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GoogleMerchant.API.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Product.ResponseModel;
using MowicoProductUpdate.API.Models;

namespace GoogleMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductParseController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductParseController(ICategoryService categoryService, IProductService productService, IHttpContextAccessor httpContextAccessor)
        {
            _productService = productService;
            _categoryService = categoryService;
            _httpContextAccessor = httpContextAccessor;
            ValidateHeader.CheckHeaderAndSetMerchantInfo(_httpContextAccessor.HttpContext.Request, _categoryService, _productService);
        }

        // GET: api/ProductParse
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ProductParse/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ProductParse
        [HttpPost]
        public Response<ProductParseResponseModel> Post([FromBody] ProductParseRequestModel request)
        {               
            if (string.IsNullOrEmpty(request.MerchantId))
            {
               throw new NullReferenceException("MerchantId is null");
            }
            if (string.IsNullOrEmpty(request.ProductXmlLink))
            {
                throw new NullReferenceException("ProductXmlLink is null");
            }
            Response<ProductParseResponseModel> response = _productService.ParseProducts(request.MerchantId, request.ProductXmlLink);
            return response;
        }

        // PUT: api/ProductParse/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
