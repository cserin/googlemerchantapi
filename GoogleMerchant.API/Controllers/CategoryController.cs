﻿using GoogleMerchant.API.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Business.Models;
using MowicoProductApi.Core.Models.Base;

namespace GoogleMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public CategoryController(ICategoryService categoryService, IProductService productService, IHttpContextAccessor httpContextAccessor)
        {
            _categoryService = categoryService;
            _productService = productService;
            _httpContextAccessor = httpContextAccessor;
            ValidateHeader.CheckHeaderAndSetMerchantInfo(_httpContextAccessor.HttpContext.Request, _categoryService, _productService);
        }

        // GET: api/v1/Category
        [HttpGet]
        public Response<CategoryResponseModel> Get()
        {
            var result = _categoryService.GetCategories();
            return result;
        }

        // GET: api/Category/{categoryId}        
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public string Get(string id)
        {            

            return null;
        }

        // POST: api/CategoryTree
        [HttpPost]
        public void Post([FromBody] string request)
        {
            //_productService.InsertProduct(request.MerchantId.ToString());
            //var result = _categoryService.GetMainCategories(request.MerchantId, request.CategoryId);            
        }

        // PUT: api/CategoryTree/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
