﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleMerchant.API.Helpers;
using GoogleMerchant.API.Models.RequestModels.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Product.RequestModel;
using MowicoProductApi.Core.Models.Products;

namespace GoogleMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductController(ICategoryService categoryService, IProductService productService, IHttpContextAccessor httpContextAccessor)
        {
            _categoryService = categoryService;
            _productService = productService;
            _httpContextAccessor = httpContextAccessor;
            ValidateHeader.CheckHeaderAndSetMerchantInfo(_httpContextAccessor.HttpContext.Request, _categoryService, _productService);
        }


        // GET: api/Product
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Product/5
        [HttpGet("{productId}")]
        public Response<ProductListModel> Get(string productId)
        {

            var result = _productService.GetProductById(productId);

            return result;
        }

        // POST: api/Product
        [HttpPost]
        public Response<ProductListModel>  Post([FromBody] ProductRequestModel request)
        {
            var result = _productService.GetProductsByCategory(request);
            return result;
        }

        // PUT: api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
