﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoogleMerchant.API.Helpers;
using GoogleMerchant.API.Models.RequestModels.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MowicoProductApi.Business.Interfaces;
using MowicoProductApi.Core.Models.Base;
using MowicoProductApi.Core.Models.Product.RequestModel;
using MowicoProductApi.Core.Models.Products;
using Newtonsoft.Json;

namespace GoogleMerchant.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductSearchController : ControllerBase
    {

        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ProductSearchController(ICategoryService categoryService, IProductService productService, IHttpContextAccessor httpContextAccessor)
        {
            _categoryService = categoryService;
            _productService = productService;
            _httpContextAccessor = httpContextAccessor;
            ValidateHeader.CheckHeaderAndSetMerchantInfo(_httpContextAccessor.HttpContext.Request, _categoryService, _productService);
        }

        // GET: api/v1/ProductSearch
        [HttpGet]
        public IEnumerable<string> Get(ProductSearchRequestModel filters)
        {
            //color:black&size:m
            //simdilik sadece name icin yapilacak.
            //var ldfjk = this.Request.Query;
            return new string[] { "value1", "value2" };
        }

        // GET: api/v1/ProductSearch/5
        /// <summary>
        /// Get product details by id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet("{productId}")]
        public Response<ProductListModel> Get(string productId)
        {
            //var result = _productService.GetProductById(productId);
            return null;
        }

        // POST: api/v1/ProductSearch
        [HttpPost]
        public ProductListModel Post([FromBody] ProductRequestModel request)
        {
            //_productService.GetProductsByCategory(request);
            return null;
        }

        // PUT: api/v1/ProductSearch/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/v1/ProductSearch/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
