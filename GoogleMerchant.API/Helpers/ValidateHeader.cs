﻿using MongoDB.Bson;
using MowicoProductApi.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Helpers
{
    public class ValidateHeader
    {
        public static void CheckHeaderAndSetMerchantInfo(Microsoft.AspNetCore.Http.HttpRequest request, IProductService productService)
        {
            string merchantId;
            if (request == null)
            {
                throw new Exception("Request is null");
            }
            if (request.Headers == null)
            {
                throw new Exception("Header is null");
            }
            if (request.Headers["MERCHANT_ID"] == string.Empty)
            {
                throw new Exception("MERCHANT_ID is null");
            }
            merchantId = request.Headers["MERCHANT_ID"];
            ObjectId objId;
            if (!ObjectId.TryParse(merchantId, out objId))
            {
                throw new Exception("MERCHANT_ID is not valid.");
            }
            productService.SetMerchantId(merchantId);
        }

        public static void CheckHeaderAndSetMerchantInfo(Microsoft.AspNetCore.Http.HttpRequest request, ICategoryService categoryService)
        {
            string merchantId;
            if (request == null)
            {
                throw new Exception("Request is null");
            }
            if (request.Headers == null)
            {
                throw new Exception("Header is null");
            }
            if (request.Headers["MERCHANT_ID"] == string.Empty)
            {
                throw new Exception("MERCHANT_ID is null");
            }
            merchantId = request.Headers["MERCHANT_ID"];
            categoryService.SetMerchantId(merchantId);
        }

        public static void CheckHeaderAndSetMerchantInfo(Microsoft.AspNetCore.Http.HttpRequest request, ICategoryService categoryService, IProductService productService)
        {
            string merchantId;
            string languageId;
            if (request == null)
            {
                throw new Exception("Request is null");
            }
            if (request.Headers == null)
            {
                throw new Exception("Header is null");
            }
            if (string.IsNullOrEmpty(request.Headers["MERCHANT_ID"]))
            {
                throw new Exception("MERCHANT_ID is null");
            }
            merchantId = request.Headers["MERCHANT_ID"];
            ObjectId objId;
            if (!ObjectId.TryParse(merchantId, out objId))
            {
                throw new Exception("MERCHANT_ID is not valid.");
            }
            if (string.IsNullOrEmpty(request.Headers["LANGUAGE_ID"]))
            {
                throw new Exception("LANGUAGE_ID is null");
            }
            languageId = request.Headers["LANGUAGE_ID"];            
            categoryService.SetMerchantId(merchantId);
            productService.SetMerchantId(merchantId);
            categoryService.SetCollectionNames(languageId);
            productService.SetCollectionNames(languageId);
        }

    }
}
