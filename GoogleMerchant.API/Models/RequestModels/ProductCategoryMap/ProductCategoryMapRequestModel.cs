﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.RequestModels
{
    public class ProductCategoryMapRequestModel
    {
        public string MerchantId { get; set; }
    }
}
