﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MowicoProductUpdate.API.Models
{
    public class ProductParseRequestModel
    {
        public string MerchantId { get; set; }

        public string ProductXmlLink { get; set; }
    }
}
