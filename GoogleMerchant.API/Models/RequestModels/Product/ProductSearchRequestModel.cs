﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoogleMerchant.API.Models.RequestModels.Product
{
    public class ProductSearchRequestModel
    {
        public string Name { get; set; }

        public string CategoryId { get; set; }
    }
}
